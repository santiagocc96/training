#!/usr/bin/env bash
# ==============================================================================
# this scripts print folders that don't contain
#   .feature & .adoc & .txt
#
# 1. your working directory must be the root of the repo training
# 2. additional to this you shall check the valid hacking challenges sites
#    http://www.wechall.net/active_sites
#
# examples
#   to list valid hacking challenges on www.root-me.org site
#   $ ./list_hacking_challs.sh rootme
#
# ==============================================================================

# grab challenge site to look for
ext="${1}"

# if no challenge site is provided it searchs in all /challenges/*

# list dirst in "./challenges/*/*"
#   usually depth 2 is enough but depth 4 because of hackthebox
dirs=$(find ./challenges -mindepth 2 -maxdepth 4 -type d | grep "$ext")

for folder in $dirs
do
  isvalid=1;

  # count feature, adoc, and txt files on this folder
  count_feature=$(find "$folder/"*".feature" 2>/dev/null | wc -w)
  count_adoc=$(find "$folder/"*".adoc" 2>/dev/null | wc -w)
  count_txt=$(find "$folder/"*".txt" 2>/dev/null | wc -w)

  # if exist feature, adoc, or txt files this chall is not valid
  if [ "$count_feature" != "0" ]; then isvalid=0; fi
  if [ "$count_adoc" != "0" ]; then isvalid=0; fi
  if [ "$count_txt" != "0" ]; then isvalid=0; fi

  # print it if valid
  if [ "$isvalid" == "1" ]; then echo "candidate found: $folder"; fi
done
