#!/usr/bin/env python
"""Remove duplicate language URL in OTHERS.lst passed as argument """
import os
import sys

VALID_EXT = ["feature", "py", "cpp", "c", "java", "cs", "js", "rb", "vb", "ml",
             "clj", "d", "hs", "sh", "php", "lua", "pas", "m", "r", "rs", "pl",
             "fs", "lisp", "cl", "f90", "erl", "pl", "bat",
             "bas", "asm", "scala", "pl6", "go"]
EXISTING = []

CURR_OTHERS = sys.argv[1]
CLEAN_OTHERS = CURR_OTHERS + "new"

with open(CURR_OTHERS, "r") as others:
    with open(CLEAN_OTHERS, "w+") as others_clean:
        for line in others:
            ext = line.split(".")[-1].strip()
            if ext in VALID_EXT:
                if ext in EXISTING:
                    print("remove repeated: " + line)
                else:
                    others_clean.write(line)
                    EXISTING.append(ext)
            else: # not valid for programming, remove
                print("remove invalid code ext: " + line)

os.replace(CLEAN_OTHERS, CURR_OTHERS)
