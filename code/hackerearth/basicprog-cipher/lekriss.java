/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

class lekriss {

  public static void main (String args[]) throws Exception{

    InputStreamReader r = new InputStreamReader(System.in);
    BufferedReader br = new BufferedReader(r);
    Scanner input = new Scanner(br);
    String toCipher = input.nextLine();
    String result = "";
    int steps = input.nextInt();
    input.close();

    for(int i=0; i<toCipher.length(); i++) {
      int comparing = toCipher.charAt(i);
      if(comparing>=65) {
        int tempSteps = steps%26;
        if(comparing<=90) {
          if(comparing+tempSteps>90) {
            result +=(char)((comparing+tempSteps)%90+64);
          }
          else result += (char)(comparing+tempSteps);
        }
        if(comparing>=97) {
          if(comparing+tempSteps>122) {
            result +=(char)((comparing+tempSteps)%122+96);
          }
          else result += (char)(comparing+tempSteps);
        }
      }
      else if(comparing>=48&&comparing<=57) {
        int tempSteps = steps%10;
        if(comparing+tempSteps>57){
          result += (char)((comparing+tempSteps)%57+47);
        }
        else result += (char)(comparing+tempSteps);
      }
      else result += toCipher.charAt(i);
    }
    System.out.println(result);
  }
}
/*
$ java lekriss
$ Epp-gsrzsCw-3-fi:Epivx5.
*/
