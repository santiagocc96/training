#! /usr/bin/env python

'''
$./charlie517.py
$ pylint charlie517.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

with open('DATA.lst') as f:
    LIGHT_X = [int(x) for x in next(f).split()][0]
    LIGHT_Y = [int(x) for x in next(f).split()][0]
    INITIAL_TX = [int(x) for x in next(f).split()][0]
    INITIAL_TY = [int(x) for x in next(f).split()][0]

    if INITIAL_TX > LIGHT_X:
        while INITIAL_TX != 0:
            print "W"
            INITIAL_TX -= 1
    elif INITIAL_TY > LIGHT_Y:
        while INITIAL_TY != 0:
            print "N"
            INITIAL_TY -= 1
    elif LIGHT_Y > INITIAL_TY:
        while LIGHT_Y != 0:
            print "S"
            LIGHT_Y -= 1
    elif LIGHT_X > INITIAL_TX:
        while LIGHT_X != 0:
            print "E"
            LIGHT_X -= 1

# ./charlie517.py
# W, N, S, E # Multiple times
