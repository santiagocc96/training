/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 23, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.util.Scanner;
import java.util.Properties;
import java.net.URL;

class Reverserange {

 URL path;
 InputStream input;
 static Properties prop;
 public Reverserange() throws Exception {
  path = ClassLoader.getSystemResource("DATA.lst");
  input = new FileInputStream(path.getFile());
  prop = new Properties();
  prop.load(input);
 }

 public static int[] Reverse(int n) {
  int b[] = new int[n];
  for (int i = Integer.parseInt(
    prop.getProperty("one")); i <= n; i++) {
   String c = Integer.toString(i);
   if (c.length() >= Integer.parseInt(prop.getProperty("two"))) {
    StringBuilder sb = new StringBuilder(c);
    String word = sb.reverse().toString().replaceFirst("^0+(?!$)", "");
    b[i - Integer.parseInt(prop.getProperty("one"))] =
    Integer.parseInt(word);
   } else {
    b[i - Integer.parseInt(prop.getProperty("one"))] = i;
   }
  }
  return b;
 }

 public void display(int a[]) {
  for (int j = Integer.parseInt(
    prop.getProperty("zero")); j < a.length; j++) {
   System.out.println(a[j]);
  }
 }

 public int sum(int a[]) {
  int sum = Integer.parseInt(
    prop.getProperty("zero"));
  for (int j = Integer.parseInt(
    prop.getProperty("zero")); j < a.length; j++) {
   sum += a[j];
  }
  return sum;
 }

 public static void main(String args[]) throws Exception {
  Reverserange p = new Reverserange();
  int res[] = Reverse(Integer.parseInt(prop.getProperty("fiftyeight")));
  p.display(res);
  int sum = p.sum(res);
  System.out.println(sum);
 }
}
/*
$ java karlhanso82
Reverse range
$
*/

