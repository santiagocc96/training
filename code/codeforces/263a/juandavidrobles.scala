object JuanDavidRobles263A {
  def main(args: Array[String]): Unit = {

    import java.util.Scanner
    import java.io.InputStreamReader
    import java.io.BufferedReader

    val streamReader: InputStreamReader = new InputStreamReader(System.in)
    val scanner: Scanner = new Scanner(new BufferedReader(streamReader))

    var coord1: Int = 0
    var coord: Int = 0

    for (i <- 0 until 5; j <- 0 until 5){
      if (Integer.parseInt(scanner.next()) == 1){
        coord = i+1
        coord1 = j+1
      }
    }

    val out: Int = Math.abs(3 - coord) + Math.abs(3 - coord1)

    println(out)
  }
}
