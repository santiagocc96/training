object JuanDavidRobles266A {
  def main(args: Array[String]): Unit = {

    /*import java.util.Scanner
    import java.io.{BufferedReader, InputStreamReader}

    val streamReader: InputStreamReader = new InputStreamReader(System.in)
    val scanner: Scanner = new Scanner(new BufferedReader(streamReader))*/

    import scala.io.StdIn

    val n: Int = /*scanner.nextInt()*/ Integer.parseInt(StdIn.readLine())
    var stones: String = /*scanner.nextLine()*/StdIn.readLine()

    var before: Char = '0'
    var count: Int = 0

    for (i <- 0 until stones.length){
      if (stones.charAt(i) == before){
        count += 1
      }
      before = stones.charAt(i)
    }

    println(count)
  }
}
