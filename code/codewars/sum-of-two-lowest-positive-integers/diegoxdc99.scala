/*
$ java -jar scalastyle_2.12-1.0.0-batch.jar
--config scalastyle_config.xml . #linting
Processed 1 file(s)
Found 0 errors
Found 0 warnings
Finished in 694 ms
$ scalac diegoxdc99.scala #compilation
*/

import scala.io.Source

object Diegoxdc99  extends App {
  var listData = Source.fromFile("DATA.lst")
    .getLines.mkString.split(" ").map(_.toInt).toList
  print(LowIntSum.sumTwoSmallest(numbers = listData))
}

object LowIntSum {
  def sumTwoSmallest(numbers: List[Int]): Int = {
    var orderList = numbers.sorted;
    orderList(0) + orderList(1)
  }
}

/*
$ scala Diegoxdc99
13
*/
