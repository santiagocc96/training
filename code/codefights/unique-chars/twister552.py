def uniqueChars(s):

    was = [False] * 26
    best = 0
    left = 0
    for right in range(0, len(s)):
        c = ord(s[right]) - ord('a')
        while was[c]:
            was[ord(s[left]) - ord('a')] = False
            left += 1
        was[c] = True
        best = max(best, right - left + 1)

    return best
