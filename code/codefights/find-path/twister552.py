def findPath(M):
    t = c = 0
    for L in M + list(zip(*M)):
        b = -1
        for a in L:
            c += a - 2 < b < a + 2
            b = a
            if a > t:
                t = a
    return c + 2 > t
