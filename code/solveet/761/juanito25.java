//Uno o dos dígitos 

import java.util.Scanner;

public class Reto2 {
    public static void main(String[] args) {
 
        Scanner digit = new Scanner(System.in);
        int numero    = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto2*");
        System.out.println("*******");
        System.out.println("");
  
        System.out.print("Ingrese por favor un numero entre 0 y 100: ");
        numero = digit.nextInt();
        System.out.println("");

        if(numero < 10) {
            System.out.println("El numero tiene un digito");
        }
        else if (numero > 10 && numero <= 100) {
            System.out.println("El numero tiene dos digitos");   
        }   
    }
} 
