//Area de un Trapecio

import java.util.Scanner;

public class Reto9 {
    public static void main(String[]args) {

        Scanner digit = new Scanner(System.in);
        int baseMayor = 0;
        int baseMenor = 0;
        int altura    = 0; 
        int area      = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto9*");
        System.out.println("*******");

        System.out.println("");
        System.out.println("*********************");
        System.out.println("*Area de un Trapecio*");
        System.out.println("*********************");
        System.out.println("");

        System.out.print("Ingrese por favor la base mayor del trapecio: ");
        baseMayor = digit.nextInt();
        System.out.println("");

        System.out.print("Ingrese por favor la base menor del trapecio: ");
        baseMenor = digit.nextInt();
        System.out.println("");
 
        System.out.print("Ingrese por favor el altura del trapecio: ");
        altura = digit.nextInt();
        System.out.println(""); 

        area = ((baseMayor+baseMenor)*altura)/2;
        System.out.println("El area del trapecio es: " + area); 
    }
}
