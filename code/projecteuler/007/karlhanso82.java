/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 02, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.lang.System.*;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.net.URL;

class Prime {

 public boolean isPrime(Integer num) {
  if (num == 2) {
   return true;
  }
  if (num <= 1) {
   return false;
  }
  if (num % 2 == 0) {
   return false;
  }

  int cont = 3;
  while ((cont * cont) <= num) {
   if ((num % cont == 0)) {
    return false;
   } else {
    cont += 2;
   }
  }
  return true;
 }

 public Integer numberPrime(Integer n) {
  int nPrimes = 1;
  int num = 1;
  while (nPrimes < n) {
   num = num + 2;
   if (this.isPrime(num)) {
    nPrimes++;
   }
  }
  return num;
 }

 public static void main(String args[]) {
  URL path = ClassLoader.getSystemResource("DATA.lst");
  try (InputStream input = new FileInputStream(path.getFile())) {
   Properties prop = new Properties();
   prop.load(input);
   Integer lastNumber = Integer.parseInt(prop.get("lastnumber").toString());
   Prime num = new Prime();
   System.out.println(prop.get("lastnumber"));
   System.out.println(prop.get("msg"));
   System.out.print(num.numberPrime(lastNumber));
  } catch (IOException e) {
   e.printStackTrace();
  }
 }
}
/*
$ java karlhanso82
Prime Number
$
*/

