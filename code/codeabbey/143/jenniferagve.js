/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function euclideanCalc(dividend, divisor, sTotal, tTotal, highComp) {
/* Here are calculated the Euclidean Algorithm and each value is updated*/
  const sPrev = sTotal[parseInt('0', 10)];
  const sCur = sTotal[parseInt('1', 10)];
  const tPrev = tTotal[parseInt('0', 10)];
  const tCur = tTotal[parseInt('1', 10)];
  const quotient = Math.floor(dividend / divisor);
  const remainder = dividend - (quotient * divisor);
  const sNext = sPrev - (quotient * sCur);
  const tNext = tPrev - (quotient * tCur);
  if (remainder === 0) {
    if (highComp === true) {
      const output = process.stdout.write(`${ divisor } ${ sCur } ${ tCur } `);
      return output;
    }
    const output = process.stdout.write(`${ divisor } ${ tCur } ${ sCur } `);
    return output;
  }
  return euclideanCalc(divisor, remainder, [ sCur, sNext ], [ tCur, tNext ],
    highComp);
}

function dataManagement(element) {
/* Here are initialized all the values and are passed to the function that
calculates the algorithm for each case given*/
  const segmentation = element.split(' ');
  const numberConvert = segmentation.map((eachNumber) => Number(eachNumber));
  const dividend = ((numberConvert[0] > numberConvert[1]) ? numberConvert[0] :
    numberConvert[1]);
  const highComp = ((numberConvert[0] > numberConvert[1]) === 1);
  const divisor = ((numberConvert[0] > numberConvert[1]) ? numberConvert[1] :
    numberConvert[0]);
  const sPrev = 1;
  const sCur = 0;
  const sTotal = [ sPrev, sCur ];
  const tPrev = 0;
  const tCur = 1;
  const tTotal = [ tPrev, tCur ];
  const euclidean = euclideanCalc(dividend, divisor, sTotal, tTotal, highComp);
  return euclidean;
}

function interpreter(erro, contents) {
/* The data of the file is processed and segmented to be processed*/
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputData = inputFile.slice(1);
  const finalValues = inputData.map((element) => dataManagement(element));
  return finalValues;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read all the data from the cards*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    interpreter(erro, contents));
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:3
23 7
31132 24144
22444 83452
-------------------------------------------------------------------------
output:1 -3 10
4 1807 -2330
124 264 -71
*/
