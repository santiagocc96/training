using System;
using System.Collections;

public class Codeabbey
{
        private static ArrayList array = new ArrayList();
        private static String result = "";
        public static void Main (){
            String text1 = "{+}x(v)<g(%)>{{< >(w)^}{}{-}[[y]/]<g>}<e><u>";
            String text2 = "({a}(b))[[ ]a]<v{(-)v}[%{c}[v{*}]{%}]>{(b) (-)(<( )-( )>c)}";
            String text3 = "[(u)<f>h]()((e)[g< >]t{[g<h>]<<^><%><a>z>(%)c[a]})(*)";
            String text4 = "[(<f>{<y>v}<(x)c>%)[f]u](*)[ <h>{(*{g}[ ](g))%}(c)";
            String text5 = "[[(-)[u] ](v)uh)((z) )[e][w](g((()c)[%<*>])][z(h)<%>]";
            String text6 = "{[{v}b][([ ]v{w})h]<f>(d){/}[a](+( ))[*] {/}}()";
            String text7 = "[[v]{f}{w[-]}h[a]<(z){f}g({^}[f< {%}><x>]e)>][<u<" 
                    +"<z>>>-]</><>(y)";
            String text8 = "{<h> u>{d}(%[v]{<a>e})%}[{d}w<(+)<^>e>][c{*}]{}";
            String text9 = "[x]{[(u)x{%}][ ]<b>[u{%{b)}]<b>(<b>(w) )}(f}";
            String text10 = "v[ ]>[<h[x]>w(y)](<d>d)(<(+)[x]([<e>+< >]+))";
            String text11 = "()<%><t(%<w>[(g)h][z]<*>(<e>[h]f){(d)z<t>})>";
            String text12 = "[*{f{[-]y}}(d<a>)({u}[u][u]<d><h>y{w[w]})]< >{}[b]<%</>"
                    +"<(e h>>";
            String text13 = "<[x][^[(<z>/<v<y><u>>)](u)y][e]{(*)^}[z[u]]<->{h}>";
            String text14 = "[f]<}>[[(a)[^[[z]y<(u) ><y><^{%d>]]^{x}]]<->";
            String text15 = "()[/]({d}b)<[+]{v}(^[v]{e})+[a]>{f}<t>(f[w(g(z<e>))<(g)->])";
            String text16 = "</>(b{{f}c})[%][{(%)a}{<->{w}g [d]< >(^)h]}][z](*){/}";
            corrFunc(text1);
            corrFunc(text2);
            corrFunc(text3);
            corrFunc(text4);
            corrFunc(text5);
            corrFunc(text6);
            corrFunc(text7);
            corrFunc(text8);
            corrFunc(text9);
            corrFunc(text10);
            corrFunc(text11);
            corrFunc(text12);
            corrFunc(text13);
            corrFunc(text14);
            corrFunc(text15);
            corrFunc(text16);
            Console.WriteLine(result);
        }

        private static void corrFunc(String text){
            if(array.Count>0){
                array.Clear();
            }
            Boolean corr = true;
            for(int i = 0;i < text.Length && corr;i++){
                if(!addElem(text[i]+"")){
                    corr = false;
                }
            }
            if(!(array.Count== 0)){
                corr = false;
            }
            if(corr){
                result += "1 ";
            }else{
                result += "0 ";
            }            
        }
        public static Boolean addElem(String text){
            Boolean addExist = true;
            if(text.Equals("[") || text.Equals("{") || text.Equals("(") || text.Equals("<")){
                array.Add(text);
            }else{
                addExist = existPar(text);
            }
            return addExist;
        }
        public static Boolean existPar(String textValidate){
            Boolean exist = false;
            String temp = inve(textValidate);
            if(!temp.Equals("")){
                if(array.Count>0){
                int i = array.Count-1;
                    if(array[i].Equals(temp)){
                        array.RemoveAt(i);
                        return true;
                    }   
                }
            }else{
                exist = true;
            }
            return exist;
        }

        public static String inve (String textValidate){
            if (textValidate.Equals(")")){
                return "(";
            }
            if (textValidate.Equals("}")){
                return "{";
            } 
            if (textValidate.Equals("]")){
                return "[";
            }
            if(textValidate.Equals(">")){
                return "<";
            }
            return "";
        }
}
