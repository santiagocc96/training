#!/usr/bin/env python3
"""
Problem #5 Minium Of Three
"""
class MinOfThree:
    """
    More practice with conditionals, comparing 3 values
    and getting the minimum value of all.
    """
    min_list = []
    values = []
    while True:

        line = input()

        if line:
            if len(line) < 3:
                pass
            else:
                values = line.split()
                if int(values[0]) < int(values[1]) and int(values[0]) < int(values[2]):
                    min_list.append(values[0])

                elif int(values[1]) < int(values[0]) and int(values[1]) < int(values[2]):
                    min_list.append(values[1])

                elif int(values[2]) < int(values[0]) and int(values[2]) < int(values[1]):
                    min_list.append(values[2])
        else:
            break

    text = ' '.join(min_list)
    print(text)

MinOfThree()
