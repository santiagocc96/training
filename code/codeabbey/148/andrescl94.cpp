#include <iostream>

using namespace std;

void read_vals(int numw,long long int *n,long long int *p,long long int *pe,
long long int *pk, long long int *c){
    int i;
    cin>>*n>>*p>>*pe;
    for(i=0;i<numw;i++){
        cin>>*(pk+i)>>*(c+i);
    }
}

long long int mod_exp(int flag,long long int n,long long int p,long long int
 pe,long long int e){
    long long int base=p,i;
    for(i=2;i<=e;i++){
        p=(p*base)%n;
        if((p==pe)&&(flag==1)){
            return i;
        }
    }
    return p;
}

long long int mod_inv(long long int x,long long int n){
    long long int t=0,t1=1,r=n,r1=x;
    while(r1!=0){
        long long int q=r/r1, taux=t1, raux=r1;
        t1=t-q*t1; t=taux;
        r1=r-q*r1; r=raux;
    }
    if(t<0){
        t+=n;
    }
    return t;
}

void decode(int numw,long long int n, long long int *pk, long long int *c,
long long int e){
    int i;
    for(i=0;i<numw;i++){
        long long int pke=mod_exp(0,n,*(pk+i),0,e);
        *(c+i)=(*(c+i)*mod_inv(pke,n))%n;
    }
    i=0;
    while(i<numw){
        cout<<(char)((*(c+i))/29791+97); *(c+i)%=29791;
        cout<<(char)((*(c+i))/961+97); *(c+i)%=961;
        cout<<(char)((*(c+i))/31+97); *(c+i)%=31;
        cout<<(char)((*(c+i))+97);
        cout<<" ";
        i++;
    }
}

int main(void){
    int numw;
    cin>>numw;
    long long int n, p, pe, pk[numw], c[numw],e;
    read_vals(numw, &n, &p, &pe, &pk[0], &c[0]);
    e=mod_exp(1,n,p,pe,n);
    decode(numw,n,&pk[0],&c[0],e);
    return 0;    
}
