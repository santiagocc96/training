/*
$ dub run dscanner -- -S simongomez95.d
Building package dscanner in /Users/nekothecat/.dub/packages/dscanner-0.5.11/ds
canner/
Running pre-generate commands for dscanner...
Performing "debug" build using /Library/D/dmd/bin/dmd for x86_64.
stdx-allocator 2.77.2: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for configuration "unittest" is up to dat
e.
libdparse 0.9.8: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../../../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -S s
imongomez95.d

$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

void main() {
  File file = File("DATA.lst", "r");
  string countstring = file.readln();
  countstring = stripws(countstring);
  const int count = to!int(countstring);
  string answer;
  int a;
  int b;
  for(int i=0; i<count; i++) {
    string[] numberstring = file.readln().split(" ");
    a = to!int(stripws(numberstring[0]));
    b = to!int(stripws(numberstring[1]));
    if(a > b) {
      answer = answer ~ " " ~ to!string(b);
    } else {
      answer = answer ~ " " ~ to!string(a);
    }
  }
  writeln(answer);
  file.close();
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*

$ ./simongomez95
 -2605178 -8641437 -1673153 -8560512 391090 -6006084 -446852 -7897953 5695945
 6727636 -6232151
 320213 -8684553 -7369740 -4312186 3082635 -9672707 -7403785 -1345861 -9665427
 -8571476 -5498134
 1971156 3278711 1395820 1187365 -6111861 -4366784
*/
