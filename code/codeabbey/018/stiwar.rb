r = 1
out = []
X = []
N = []
puts 'formato  [X N] => X: radicando N: numero de iteraciones'
puts '**********************************************************'
puts ''
puts 'ingrese el numero de pruebas:'
p = gets.chomp.to_i

c=0
(1..p).each do
  data = []
  puts 'ingresa dos numeros separados por un espacio:'
  data = gets.chomp.split(" ")
  X[c] = data[0].to_i
  N[c] = data[1].to_i
  c+=1;
end 

i = 0
while i < p
  
  (1..N[i]).each do
    
      temp = r
      r = (X[i]/r).round(7)
      
      r = (temp + r).round(7)
     
      r = (r / 2).round(7)
  end
  out[i] = r
  r = 1  
  i+=1
end
out.map(&:inspect).join(" ")
