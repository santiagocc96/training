'''
Problem #101 Gradient Calculation
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function
import math


def func(x_co, y_co, a_const, b_const, c_const):
    '''
    Returns the f value for the given variables.
    '''
    val = (x_co - a_const)**2 + (y_co - b_const)**2 + c_const * math.exp(
        -(x_co + a_const)**2 - (y_co + b_const)**2)
    return val


def gradient(x_co, y_co, a_const, b_const, c_const):
    '''
    Returns the gradient value of the points with the given constants.
    '''
    dit = 0.000000001
    func_1 = func(x_co, y_co, a_const, b_const, c_const)
    func_2 = func(x_co + dit, y_co, a_const, b_const, c_const)
    func_3 = func(x_co, y_co + dit, a_const, b_const, c_const)
    x_grad = (func_2 - func_1) / dit
    y_grad = (func_3 - func_1) / dit
    return [x_grad, y_grad]


def slope(grad):
    '''
    Returns the direction in which the slope raises most steeply.
    '''
    return round(math.degrees(math.atan2(grad[1], grad[0])))


def main():
    '''
    Main function that reads and processes all the points in order to return
    their gradient value.
    '''
    d_file = open("DATA.lst", "r")
    points, a_const, b_const, c_const = map(
        float, [float(x) for x in d_file.readline().split()])
    points = int(points)
    answers = []
    for _ in range(points):
        x_co, y_co = map(float, [float(x) for x in d_file.readline().split()])
        slo = slope(gradient(x_co, y_co, a_const, b_const, c_const))
        answers.append(slo + 180)
    print(" ".join(str(x) for x in answers))


main()

# pylint: disable=pointless-string-statement,invalid-name

'''
$ py develalopez.py
323 295 28 148 284 319 259 330 5 290 278 319 275 273 325
'''
