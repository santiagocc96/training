#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

/* eslint-disable fp/no-arguments */
const [ input ] = arguments;
/* eslint-enable fp/no-arguments */

const lines = input.split(/\n/);

const [ total ] = lines;
const matches = lines.slice(1);

const rules = [ 'RS', 'SP', 'PR' ];

function chooseWinner(current) {
  if (current[0] === current[1]) {
    return 0;
  }
  return rules.includes(current) ? 1 : 2;
}

function matchWinners(index = 0, winners = []) {
  if (index > total - 1) {
    return winners;
  }
  const match = matches[index].split(/\s/);
  const results = match.map(chooseWinner);
  const one = results.filter((current) => current === 1).length;
  const two = results.filter((current) => current === 2).length;
  return matchWinners(index + 1, winners.concat(one > two ? 1 : 2));
}

/* eslint-disable fp/no-unused-expression*/
print(matchWinners().join(' '));
/* eslint-enable fp/no-unused-expression*/

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
1 2 1 2 1 2 2 1 2 2 2 1 2 1 1 1 1 1 2 2 2
*/
