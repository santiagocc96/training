<?php
/**
 * Fibonacci Divisibility Advanced
 *
 * PHP version 7.2.10
 * phpcs dianaosorio97.php
 *
 * @category Challenge
 * @package  Fibonacci
 * @author   Diana Osorio <dianaosorio66@gmail.com>
 * @license  GNU General Public License
 * @link     none
 */

if (file_exists('DATA.lst')) {
    $file = fopen("DATA.lst", "r");
    $array = file("DATA.lst");
    $numbers = explode(" ", $array[1]);
    foreach ($numbers as $k) {
        $n3 = 0;
        $n1 = 0;
        $n2 = 1;
        $i = 2;
        do {
            $n3 =($n1+$n2)%$k;
            $n1 = $n2;
            $n2 = $n3;
            // calculates the number module
            $lim = bcmod($n3, $k);
            if ($lim==0) {
                //print answer
                echo  $i." ";
                break;
            }
            $i++;
        } while ($lim!=0);
    }


    echo "\n";
} else {
    echo "Fail";
}


/*
php dianaosorio97.php
output:
158244 209048 73308 12654 63924 383084 66300
1776 11364 7080 195769 233172 138793 101610
74298 57057 3480 1015890 43370 1548852
*/
?>
