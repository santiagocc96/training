;; $ lein eastwood
;;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;;   Directories scanned for source files:
;;     src test
;;   == Linting kamadoatfluid.clj ==
;;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

;; namespace
(ns kamadoatfluid.clj
  (:gen-class))

;; parses a string into a double
(defn todbl [s]
  (Double/parseDouble s))

;; load lines of file handled by 'rdr' into a vector of double
(defn loadf [rdr]
  (vec (line-seq rdr)))

;; tokenize whitespace separated string into a vector of double
(defn get-tokens-vect [s]
  (vec (map #(todbl %) (clojure.string/split s #" "))))

;; get the magnitude from two vectors
(defn absm [x y]
  (Math/hypot x y))

;; get the polar angle given two vectors
(defn angle [x y]
  (Math/atan2 y x))

;; get the horizontal component of vector 'm' and polar angle 'a'
(defn geth [a m]
  (* m (Math/cos a)))

;; get the vertical component of vector 'm' and polar angle 'a'
(defn getv [a m]
  (* m (Math/sin a)))

;; returns the position when the ball stops
(defn simulate [_W _H _x _y _R _vx _vy _A]
  (with-local-vars [W   _W
                    H   _H
                    R   _R
                    F   _A
                    dt  0.001
                    xc  _x
                    yc  _y
                    al  (angle _x _y)
                    vx  _vx
                    vy  _vy
                    vt  (absm _vx _vy)]
    (while (> @vt 0.0)
      (var-set xc (+ @xc (* @vx @dt)))
      (var-set yc (+ @yc (* @vy @dt)))
      (if (or (> (+ @xc @R) @W) (< (- @xc @R) 0.0))
        (do
          (var-set vx (* @vx -1.0))
          (var-set xc (+ @xc (* 2.0 (* @vx @dt))))))
      (if (or (> (+ @yc @R) @H) (< (- @yc @R) 0.0))
        (do
          (var-set vy (* @vy -1.0))
          (var-set yc (+ @yc (* 2.0 (* @vy @dt))))))
      (var-set vt (- @vt (* @F @dt)))
      (var-set al (angle @vx @vy))
      (var-set vx (geth @al @vt))
      (var-set vy (getv @al @vt))
    )
    (println (Math/round @xc) (Math/round @yc))
  )
)

;; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (let [f  (loadf rdr)
          l  (get-tokens-vect (get f 0))
          W  (get l 0)
          H  (get l 1)
          x  (get l 2)
          y  (get l 3)
          R  (get l 4)
          vx (get l 5)
          vy (get l 6)
          A  (get l 7)]
        (simulate W H x y R vx vy A))))

;; entry point
(defn -main [& args]
  (process_file "DATA.lst"))

;; $lein run
;; 136 176
