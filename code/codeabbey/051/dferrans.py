"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting
"""
from __future__ import print_function


def dungeons_dragons_dice(data):
    """Function to get best dices for a given array."""
    resultlist = []
    data = 3
    for _ in range(data):
        dice_results = list(map(int, input().split()))
        dice_results.pop(-1)

        dictionary = {
            "1d2": (100, 200, 1, 4),
            "1d4": (100, 400, 1, 4),
            "1d6": (100, 600, 1, 6),
            "1d8": (100, 800, 1, 8),
            "1d10": (100, 1000, 1, 10),
            "1d12": (100, 1200, 1, 12),
            "2d2": (200, 400, 2, 4),
            "2d4": (200, 800, 2, 8),
            "2d6": (200, 1200, 2, 12),
            "2d8": (200, 1600, 2, 16),
            "2d10": (200, 2000, 2, 20),
            "2d12": (200, 2400, 2, 24),
            "3d2": (300, 600, 3, 6),
            "3d4": (300, 1200, 3, 12),
            "3d6": (300, 1800, 3, 18),
            "3d8": (300, 2400, 3, 24),
            "3d10": (300, 3000, 3, 30),
            "3d12": (300, 3600, 3, 36),
            "4d2": (400, 800, 4, 8),
            "4d4": (400, 1600, 4, 16),
            "4d6": (400, 2400, 4, 24),
            "4d8": (400, 3200, 4, 32),
            "4d10": (400, 4000, 4, 40),
            "4d12": (400, 4800, 4, 48),
            "5d2": (500, 1000, 5, 10),
            "5d4": (500, 2000, 5, 20),
            "5d6": (500, 3000, 5, 30),
            "5d8": (500, 4000, 5, 40),
            "5d10": (500, 5000, 5, 50),
            "5d12": (500, 6000, 5, 60)
        }

        val = sum(dice_results)
        maxv = max(dice_results)
        minv = min(dice_results)
        mostlikely = ""
        initprob = 0.0
        for key, vlo in dictionary.items():
            if minv >= vlo[2] and maxv <= vlo[3] and val >= vlo[0]:

                probvalues = key.split('d')
                probfinal = (1/int(probvalues[1])) * int(probvalues[0])

                if probfinal > initprob:
                    mostlikely = key
                    initprob = probfinal

        initprob = 0.0
        resultlist.append(mostlikely)
    print(" ".join(resultlist))


dungeons_dragons_dice(input)
# $ python dferrans.py < DATA.lst
# 2d8 5d6 2d6
