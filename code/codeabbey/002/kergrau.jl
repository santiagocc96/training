#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA2.lst") do file
  flag = false
  answer = 0
  number = 0
  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end
    for i in split(ln, " ")
      number = parse(Int64, i)
      answer = answer + number
    end
    println(answer)
  end
end

# $julia kergrau.jl
# 31752
