#! /usr/bin/env ruby

# $ ruby charlie517.rb
# $ rubocop charlie517.rb
# 1 file inspected, no offenses detected

File.readlines('DATA.lst').drop(1).each do |line|
  data = line.split(' ').map(&:to_i)
  puts data.inject(0) { |sum, i| sum + i }
end

# $ ruby charlie517.rb
# 31752
