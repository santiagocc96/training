#!/usr/bin/env python3
"""
Problem #2 Sum In Loop
"""
class SumInLoop:
    """
    Learning loops
    """
    sum = 0
    n_values = input()
    test_values = input().split()
    for i in range(int(n_values)):

        sum += int(test_values[i])

    print (sum)

SumInLoop()
