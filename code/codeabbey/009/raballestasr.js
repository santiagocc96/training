"use strict";

/**
 * Determines whether a triangle with given sides can be built
 * based on the triangular inequality, i.e., if the sum of each
 * pair of sides is strictly larger than the other side.
 * @param {number} s1 The length of side 1.
 * @param {number} s2 The length of side 2.
 * @param {number} s3 The length of side 3.
 * @return {number} 1 if the triangle can be built, 0 if not.
*/
function canBuildTriangle(s1, s2, s3) {
    var a = Math.min((s2 + s3) - s1, (s1 + s3) - s2, (s1 + s2) - s3);
    return Number(a > 0);
}

var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += canBuildTriangle(Number(line[0]), Number(line[1]),
        Number(line[2])) + " ";
}
console.log(salida);
