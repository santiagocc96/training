print_string "ingrese el tamano del array: \n";;
let tamano = read_int ();;
print_string "ingrese los numeros separados por un espacio: \n";;
let data = read_line ();;
print_string "\n";;
let intList = List.map int_of_string(Str.split (Str.regexp " ") data);;
let dataArray = Array.make  tamano  (-1);; (*valores iniciales para el array*)
let out = Array.make  (tamano-1)  (-1);;

for i=0 to ((Array.length dataArray) -1) do
    dataArray.(i) <- (List.nth intList i);
done;;
let c = ref 0;;
let count = ref 0;;
let p = ((Array.length dataArray) - 1);;
let po = ref 1;;
while !c < p do
  count := 0;
  for j=1 to !po do (*para cada iteracion creciente del array debemos ordenar y contar*)
    for m = !po downto 1 do
      let t = dataArray.(m) in
        if(dataArray.(m-1)>dataArray.(m)) then(
            dataArray.(m)<-dataArray.(m-1);
            dataArray.(m-1)<-t;
            count := !count + 1;
            out.(!c) <- !count;
        )else (out.(!c) <- !count;)
    done;
  done;
  c := !c + 1;
  po := !po + 1;
done;;
for i=0 to ((Array.length out) - 1) do
  if( i!=(Array.length out) - 1 ) then Printf.printf "%i " out.(i) else Printf.printf "%i \n" out.(i) ;
done;;
