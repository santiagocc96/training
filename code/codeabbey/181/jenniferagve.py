"""
  Feature: Solving the challenge #181 Reverse Polish Notation
  With Python v3
  From http://www.codeabbey.com/index/task_view/reverse-polish-notation


 Linting:   pylint jenniferagve.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
import math


def operation(value, var1, var2):
    """On this function math operations are made taking 2 values"""
    if value == "mul":
        result = float(var1) * float(var2)
    elif value == "add":
        result = float(var1) + float(var2)
    elif value == "sub":
        result = float(var1) - float(var2)
    elif value == "div":
        result = float(var1) / float(var2)
    elif value == "sqrt":
        result = math.sqrt(float(var2))
    elif value == "mod":
        result = float(var1) % float(var2)

    return result


def calculate():
    """This function goes through the data, taking and defining the
        values from the array to calculate the values"""
    data = open('DATA.lst', "r")
    data = data.readline()
    data = data.split()
    length = len(data)
    number = 0
    while length != 1:
        if data[number] == "mul" or data[number] == "add":
            comp = 1
        elif data[number] == "sub" or data[number] == "sqrt":
            comp = 1
        elif data[number] == "div" or data[number] == "mod":
            comp = 1
        else:
            comp = 0

        if comp == 1:
            var1 = data[number - 2]
            var2 = data[number - 1]

            if data[number] == "sqrt":
                var2 = data[number - 1]
                data2 = data[0:number - 1]
            else:
                data2 = data[0:number - 2]
            result = operation(data[number], var1, var2)
            data2.append(str(result))
            data2.extend(data[(number + 1):length])
            data = data2
            length = len(data)
            number = 0
        else:
            number += 1

        total = int(round(float(data[0])))
    print total


calculate()

# pylint: disable=pointless-string-statement

''' python jenniferagve.py
    input: 70 11 mul 5 div 219 add 28 26 6 sub 6 sub div mul 448 7 mul sqrt add
    output: 802'''
