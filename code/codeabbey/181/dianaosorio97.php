<?php
/**
 *$ phpcs dianaosorio97.php #linting
 *$ php dianaosorio97.php  #Compilation
 *
 */

if (file_exists('DATA.lst')) {
  $file = fopen("DATA.lst", "r");
  $data = file("DATA.lst");
  $postfix = explode(" ", $data[0]);
  $stack = new SplStack();
  for ($i=0; $i < count($postfix); $i++) {
    if ($postfix[$i]!= 'mul') {
      if ($postfix[$i] != 'add') {
        if ($postfix[$i] != 'sub') {
          if ($postfix[$i] != 'mod') {
            if ($postfix[$i] != 'sqrt') {
              if ($postfix[$i] != 'div') {
                settype($postfix[$i], "integer");
              }
            }
          }
        }
      }
    }
  }

  foreach ($postfix as $key) {
    $j=0;
    $a = array();
    if (gettype($key)== "integer") {
      $stack->push($key);
    }
    else {
      if ($key == 'add') {
        $stack->rewind();
        while ($j<2) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = $a[0]+$a[1];
        $stack->push($aux);
        $a = 0;
      }
      else if ($key == 'mul') {
        $stack->rewind();
        while ($j<2) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = $a[1]*$a[0];
        $stack->push($aux);
        $a = 0;
      }
      else if ($key == 'sub') {
        $stack->rewind();
        while ($j<2) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = $a[1]-$a[0];
        $stack->push($aux);
        $a = 0;
      }
      else if ($key == 'div') {
        $stack->rewind();
        while ($j<2) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = $a[1]/$a[0];
        $stack->push($aux);
        $a = 0;
      }
      else if ($key == 'mod') {
        $stack->rewind();
        while ($j<2) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = $a[1] % $a[0];
        $stack->push($aux);
        $a = 0;
      }
      else if ($key == 'sqrt') {
        $stack->rewind();
        while ($j<1) {
          array_push($a, $stack->pop());
          $stack->next();
          $j++;
        }
        $aux = sqrt($a[0]);
        $stack->push($aux);
        $a = 0;
      }
    }
  }
  foreach ($stack as $k) {
    echo $k;
  }
  echo "\n";
} else {
    echo "Fail";
}

#$php dianaosorio97.php
#output:
#927
?>
