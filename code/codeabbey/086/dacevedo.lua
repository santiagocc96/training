function main()
    
    inputData = {}
    memory = {}
    inputIndex = 0
    memIndex = 0
    lastOpenBracketPos = 0
    
    bfckCode = {}
    
    instrCount = 0
    for i in string.gmatch(io.read(), ".") do
        bfckCode[instrCount] = i 
        instrCount = instrCount + 1
    end
    io.flush()
    
    inputSplit = string.gmatch(io.read(), "%S+")
    io.flush()
    
    for i=0, 1024 do
      memory[i] = 0
    end
    
    j = 0
    for i in inputSplit do
        inputData[j] = tonumber(i)
        j = j + 1
    end
    
    i = 0

    while i <= instrCount do
        if bfckCode[i] == ";" then
            memory[memIndex] = inputData[inputIndex]
            inputIndex = inputIndex + 1
        elseif bfckCode[i] == ">" then
            memIndex = memIndex + 1
        elseif bfckCode[i] == "<" then
            memIndex = memIndex - 1
        elseif bfckCode[i] == "[" then
            lastOpenBracketPos = i
            if memory[memIndex] == 0 then
                for j=i, instrCount do
                    if bfckCode[j] == "]" then
                        i = j
                        break
                    end
                end
            end
        elseif bfckCode[i] == "]" then
            i = lastOpenBracketPos - 1
        elseif bfckCode[i] == "-" then
            memory[memIndex] = memory[memIndex] - 1
        elseif bfckCode[i] == "+" then
            memory[memIndex] = memory[memIndex] + 1
        elseif bfckCode[i] == ":" then
            io.write (memory[memIndex] .. " ")
            io.flush()
        end
        i = i + 1
        
    end
end

main()
