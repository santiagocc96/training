/*
$ eslint jarboleda.js
$
*/

function numerationGenerator(currentArray, index, maxValue) {
  if (index >= maxValue) {
    return currentArray;
  }
  return numerationGenerator(currentArray.concat(index), index + 1, maxValue);
}

const numberOfColumns = 8;
const zero = 0;
const one = 1;

const theOriginarArray = numerationGenerator([], 0, numberOfColumns - 1);

function getVertical(queenPosition) {
  const column = queenPosition.split('')[zero];
  const tempArray = theOriginarArray
    .map((value) => (value + 1))
    .map((value) => (column.concat(value)));
  return tempArray;
}

const theInitialValueOfTheCoumn = 97;

function getHorizontal(queenPosition) {
  const row = queenPosition.split('')[one];
  const tempArray = theOriginarArray
    .map((value) => (String.fromCharCode(theInitialValueOfTheCoumn + value)
      .concat(row)));
  return tempArray;
}

function getTopRightToBottomLeft(queenPosition) {
  const column = queenPosition.split('')[0].charCodeAt(0) -
    theInitialValueOfTheCoumn;
  const row = queenPosition.split('')[1] - 1;
  const columns = theOriginarArray;
  const rows = theOriginarArray;
  const diff = column - row;
  if (diff > 0) {
    const columnsPrime = columns.map((value) => (value + diff))
      .slice(0, -diff);
    const cells = columnsPrime.map((value, index) =>
      (String.fromCharCode(theInitialValueOfTheCoumn + value)
        .concat(rows[index] + 1)));
    return cells;
  }
  /* else */
  const rowsPrime = rows.map((value) => (value - diff)).slice(0, diff);
  const cells = rowsPrime.map((value, index) =>
    (String.fromCharCode(theInitialValueOfTheCoumn + columns[index])
      .concat(value + 1)));

  return cells;
}

function getTopLeftToBottomRight(queenPosition) {
  const column = queenPosition.split('')[0].charCodeAt(0) -
    theInitialValueOfTheCoumn;
  const row = queenPosition.split('')[1] - 1;
  const columns = theOriginarArray
    .map((value) => (numberOfColumns - value - 1));
  const rows = theOriginarArray;
  const diff = (numberOfColumns - column - 1) - row;
  if (diff > 0) {
    const columnsPrime = columns.map((value) => (value - diff)).slice(0, -diff);
    const cells = columnsPrime.map((value, index) =>
      (String.fromCharCode(theInitialValueOfTheCoumn + value)
        .concat(rows[index] + 1)));
    return cells;
  }
  /* else */
  const rowsPrime = rows.map((value) => (value - diff)).slice(0, diff);
  const cells = rowsPrime.map((value, index) =>
    (String.fromCharCode(theInitialValueOfTheCoumn +
        rows[numberOfColumns - index - 1])
      .concat(value + 1)));
  return cells;
}

function solveForLine(positions) {
  /* King's first */
  const vertical = getVertical(positions[1]);
  const horizontal = getHorizontal(positions[1]);
  const topLeftToBottomRight = getTopLeftToBottomRight(positions[1]);
  const topRightToBottomLeft = getTopRightToBottomLeft(positions[1]);
  const queenPosiblePositions = vertical.concat(horizontal)
    .concat(topLeftToBottomRight).concat(topRightToBottomLeft);
  if (queenPosiblePositions.includes(positions[0])) {
    return 'Y';
  }
  return 'N';
}


function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const numberOfLines = contents.split('\n')[zero];
  const splited = contents.split('\n').map((line) => (line.split(' ')))
    .slice(1, numberOfLines + 1);
  const answer = splited.map((line) => (solveForLine(line)))
    .join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
N N N N N N N N N N N N Y N Y N Y N N N Y N N N Y
*/
