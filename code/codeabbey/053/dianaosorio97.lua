--[[
    $ luacheck dianaosorio97.lua #linter
    Checking dianaosorio97.lua                       OK
    Total: 0 warnings / 0 errors in 1 file
    $ lua dianaosorio97.lua      #compilation
]]--

local file = io.open("DATA.lst")
local answer = ""
local nline = 0
local c1 = 0
local c2 = 0
local f1 = 0
local f2 = 0
local column = 0
local letters = {[1]="a",[2]="b",[3]="c",[4]="d",[5]="e",
                [6]="f",[7]="g",[8]="h"}
for line in file:lines() do
  nline = nline + 1
  if nline ~= 1 then
    for i in string.gmatch(line, "%w+") do
      column = column + 1
      for token in string.gmatch(i, "%S") do
        if column == 1 then
          for index, value in ipairs(letters) do
            if value == token then
              f1 = index
            else
              c1 = tonumber(token)
            end
          end
        elseif column == 2 then
          for index, value in ipairs(letters) do
              if value == token then
                f2 = index
              else
                c2 = tonumber(token)
              end
          end
        end
      end
    end
    if f1==f2 or c1== c2 then
      answer = "Y"
    elseif math.abs(f1- f2) == math.abs(c1-c2) then
      answer = "Y"
    elseif math.abs(f1+f2) == math.abs(c1+c2) then
      answer = "Y"
    else
      answer = "N"
    end
    io.write(answer.." ")
    column = 0
  end
end
print("\n")

--[[
    $ lua dianaosorio97.lua
    output:
    N Y N Y N Y N N N Y N N Y Y N N Y Y Y N Y Y N
]]--

