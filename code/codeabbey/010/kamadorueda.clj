; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; tokenize whitespace separated string into a vector of integers
(defn stoi
  ([string]
    (map #(Integer/parseInt %)
      (clojure.string/split string #" ")
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [l (stoi line)
              v (into [] l)]
          (if (= 4 (count l))
            (let [xa (get v 0)
                  ya (get v 1)
                  xb (get v 2)
                  yb (get v 3)
                  m  (Math/round (double (/ (- yb ya) (- xb xa))))
                  b  (Math/round (double (- ya (* m xa))))]
              (print (str "(" m " " b ") "))
            )
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   (-78 -834) (-48 -353) (95 621) (58 -495) (91 -681) (-60 254) (-10 430)
;   (-32 -645) (-48 -855) (-21 -279) (-66 520) (-97 -212) (11 268) (-48 -699)
;   (73 -977)
