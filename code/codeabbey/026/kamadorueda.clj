; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kamadoatfluid.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kamadoatfluid.clj
  (:gen-class)
)

; tokenize whitespace separated string into a convenient type and container
(defn s2c [wsstr]
  (into [] (map #(Integer/parseInt %) (clojure.string/split wsstr #" ")))
)

; return the greatest common divisor between 'A' and 'B'
(defn GCD [A B]
  (loop [a A b B s 0]
    (if (= s 0)
      (if (= a 0)
        b
        (recur a (mod b a) 1)
      )
      (if (= b 0)
        a
        (recur (mod a b) b 0)
      )
    )
  )
)

; return the least common multiple between 'A' and 'B'
(defn LCM [A B]
  (/ (* A B) (GCD A B))
)

; parse DATA.lst and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
              s  (count c)
              A  (get c 0)
              B  (get c 1)]
          (if (= s 2)
            (print (str "(" (GCD A B) " " (LCM A B) ") "))
          )
        )
      )
    )
    (println)
  )
)

; fire it all
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;  (30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
;  (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641)
;  (1 10) (3048180) (160 184800) (2 40)
