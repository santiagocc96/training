"""
pylint -r n kergrau.py
No config file found, using default configuration
"""

import httplib2
FILE = open("DATA.lst", "r")

TOKI = "token: " + FILE.read() + "\n"
LINK_WEB = "http://open-abbey.appspot.com/interactive/say-100"
HTTP = httplib2.Http()
RESPONSE, CONTENT = HTTP.request(LINK_WEB, "POST", TOKI)

# Below is the game say one hundred
HUNDRED = 100 - int(CONTENT.split(" ")[1])
# Then, I sent my answer to server
TOKI = TOKI + "answer: " + str(HUNDRED)
RESPONSE, CONTENT = HTTP.request(LINK_WEB, "POST", TOKI)
# Server returns me a token

print CONTENT

# python kergrau.py
# T4FkO97auqTX45SezEBeUTI5
