#!/usr/bin/env bash
# $ shellcheck yamuz0.sh
# $

IFS=$'\n'
# loading lines from DATA.lst
all_lines=($(cat "DATA.lst"))
# deleting the information about amount of lines due it doesn't matter
unset all_lines[0]
all_lines=( "${all_lines[@]}" )
# initializing the array where will be saved the results of the script
results=()

# while there are remaining lines to analyze keep running
while [ ${#all_lines[@]} -ne 0 ]; do
  counting=0
  # selecting line to analyze.
  current_line=${all_lines[0]}
  # deleting at the whole array of lines, the line to be analyzed
  unset all_lines[0]
  all_lines=( "${all_lines[@]}" )
  # counting vowels in the current analyzed line
  for (( i=0; i<${#current_line}; i++ )); do case "${current_line:$i:1}" in
    a)counting=$((counting + 1));;
    e)counting=$((counting + 1));;
    i)counting=$((counting + 1));;
    o)counting=$((counting + 1));;
    u)counting=$((counting + 1));;
    y)counting=$((counting + 1));;
  esac; done
  # saving results
  results=("${results[@]}" $counting)
done

# printing amount of vowels in every line
echo "${results[@]}"

# $ ./yamuz0.sh
# 10 11 15 14 17 11 10 12 6 6 5 8 11 10 16 9
