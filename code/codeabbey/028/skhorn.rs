/*
$ rustup run nightly cargo clippy
Compiling skhorn v0.1.0 (file:///../skhorn)
Finished dev [unoptimized + debuginfo] target(s) in 0.22 secs
$ rustc skhorn.rs
$ 
*/

//Read file 
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
  let file = File::open("DATA.lst").unwrap();

  let mut output_vec = Vec::new(); 

  for line in BufReader::new(file).lines() {

    let cur_line: &str = &line.unwrap();
    let array: Vec<&str> =   cur_line.split(' ').collect();

    if array.len() > 1 {
      //println!("{:?}", array);
      let weigth: f32 = array[0].parse().unwrap();
      let heigth: f32 = array[1].parse().unwrap();

      let mut bmi:f32 = body_max_index(weigth, heigth);
      if bmi < 18.5 {
        output_vec.push("under");
      } 
      else if bmi >= 18.5 && bmi < 25.0 {
        output_vec.push("normal");
      } 
      else if bmi >= 25.0 && bmi < 30.0 {
        output_vec.push("over");
      } 
      else if bmi >= 30.0 {
        output_vec.push("obese");
      }
    }
    
  }
  let mut result: String = Default::default();
  for item in &output_vec {
    result.push(' ');
    result.push_str(item);
  }
  println!("{}", result);
}

fn body_max_index(weigth: f32, heigth: f32) -> f32 {
  let bmi: f32 = weigth / (heigth*heigth);
  bmi
}

/*
$ ./skhorn 
over obese obese normal obese obese obese obese obese under over over under under over obese over over obese under obese over normal under normal normal under over obese obese
*/
