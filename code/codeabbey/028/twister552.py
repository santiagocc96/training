data = [cadena.split(" ") for cadena in
        """35
56 1.41
58 1.55
96 2.83
81 1.52
45 1.13
111 1.83
113 1.77
53 1.33
63 2.05
110 2.14
63 1.62
75 2.40
83 2.04
67 1.64
66 1.41
80 1.79
78 1.61
42 1.67
102 2.02
46 1.18
75 1.44
95 2.18
114 1.86
46 1.70
40 1.05
96 2.31
76 2.33
69 1.37
80 1.63
80 1.56
82 2.73
70 1.38
99 2.17
53 1.49
116 2.61""".splitlines()]

no_of_people=int(data[0][0])+1
answer = []

def calc_BMI(weight, height):
    bmi = weight / height**2
    if 0 <= bmi < 18.5:
        return "under"
    elif 18.5 <= bmi < 25.0:
        return "normal"
    elif 25.0 <= bmi < 30.0:
        return "over"
    else:
        return "obese"

for i in range(1, no_of_people):
    answer.append(calc_BMI(int(data[i][0]),float(data[i][1])))
for elem in answer:
    print(elem, end=" ")

