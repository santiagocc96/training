#!/bin/bash
#
# Problem #13 Weighted sum of digits
#
function digits () {
    counter=0    
    for item in $1; 
    do
        division=$item        
        remainder=0
        count=$(echo -n "$item" | wc -c)

        while [[ "$division" -gt 0 ]];
        do
            let "remainder+=(division%10)*count"
            division=$((division/10))
            let "count-=1"
        done

        output_array[$counter]=$remainder
        let "counter+=1"
    done    

    printf '%s ' "${output_array[@]}"
}

while read -r line || [[ -n "$line" ]]; 
do
    len=$(echo -n "$line" | wc -c)
    if [[ "$len" -gt 3 ]]
    then
        digits "$line"
    fi
    
    
done < "$1"
