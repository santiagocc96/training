/*
$ golint badwolf10.go #linting
$ go tool compile -e badwolf10.go #compilation
*/

package main
import (
  "fmt"
  "math"
)


func main() {
  var N int
  fmt.Scanf("%d\n", &N)

  for i := 0; i < N; i++ {
    var pa, pb float64
    fmt.Scanf("%f %f\n", &pa, &pb)
    pa = pa / 100.0
    pb = pb / 100.0
    var Pi, P float64 = 1, 0
    var n float64
    for Pi > 0.00001 {
      Pi = math.Pow((1-pa)*(1-pb), n) * pa
      P += Pi
      n++
    }
    fmt.Print(math.Round(P*100), " ")
  }
}

/*
$ go run badwolf10.go
2
30 50
20 15

46 63
*/
