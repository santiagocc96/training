'''
➜  59 git:(sgomezatfluid) ✗ mix credo --strict sgomezatfluid.exs
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.1 seconds (0.00s to load, 0.1s running checks)
9 mods/funs, found no issues.
'''

defmodule FileReader do
  def lines(string) do
    String.split(string, "\n", trim: true)
  end

  def indiv(line) do
    inds = String.split(line, " ", trim: true)
    {{a, ""}, {b, ""}} = {Integer.parse(hd(inds)), Integer.parse(hd(tl(inds)))}
    {a, b}
  end

  def data_initial(filename) do
    file = File.read!(filename)
    file |> FileReader.lines
      |> hd
      |> indiv
  end

  def data_body(filename) do
    file = File.read!(filename)
    file  |> FileReader.lines
      |> tl |> hd
      |> String.split(" ")
  end
end

defmodule BullsCows do
  def get_hint([], [], _, hint) do
    hint
  end

  def get_hint([guess | gt], [num | nt], number, {correctpos, existing}) do
    cond do
      guess == num ->
        get_hint gt, nt, number, {correctpos + 1, existing}

      Enum.member?(number, guess) ->
        get_hint gt, nt, number, {correctpos, existing + 1}

      true ->
        get_hint gt, nt, number, {correctpos, existing}
    end
  end

  def print_hint({correctpos, existing}) do
    IO.write Integer.to_string(correctpos)
      <> "-"
      <> Integer.to_string(existing)
      <> " "
  end

end

{number, _} = FileReader.data_initial "DATA.lst"
guesses = FileReader.data_body "DATA.lst"
number = String.graphemes(Integer.to_string(number))

Enum.map guesses, fn(guess) -> guess
  |> String.graphemes
  |> BullsCows.get_hint(number, number, {0, 0})
  |> BullsCows.print_hint end

'''
➜  59 git:(sgomezatfluid) ✗ elixir sgomezatfluid.exs
1-1 0-3 0-2 0-2 1-2 0-2 1-0 0-1 1-1 1-0 1-2 0-2 2-0 0-2 0-3
'''
