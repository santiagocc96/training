/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

const nsteps = 20;
const nwalkers = 50;
const mathjs = require('mathjs');

function createGraph(msize, edges) {
  const igraph = Array.from({ length: msize }, () =>
    Array.from({ length: msize }, () => 0));
  const congraph = igraph.map((row, rowi) => row.map((elmt, colj) =>
    edges.filter((eij) => (eij[0] === rowi) && (eij[1] === colj)).length));
  const graph = congraph.map((row) => {
    const rsize = row.reduce((acc, val) => acc + val);
    return row.map((elmt) => elmt / rsize);
  });
  return graph;
}

function runGraph(graph, steps, walkers) {
  if (steps === 0) {
    return walkers.map(Math.round);
  }
  const updwalkers = mathjs.multiply(walkers, graph);
  return runGraph(graph, steps - 1, updwalkers);
}

function getPageRank(msize, edges) {
  const graph = createGraph(msize, edges);
  const walkers = Array.from({ length: msize }, () => nwalkers);
  const ranks = runGraph(graph, nsteps, walkers);
  return ranks;
}

function findPageRank(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const [ msize ] = dataLines.slice(0, 1)[0].split(' ')
    .map(Number).slice(0, 1);
  const edges = dataLines.slice(1).map((line) => line.split(' ').map(Number));
  const pagerank = getPageRank(msize, edges);
  pagerank.map((val) => console.log(val));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findPageRank(readerr, contents));
}

main();

/*
$ node badwolf10.js
73 23 37 70 73 23
*/
