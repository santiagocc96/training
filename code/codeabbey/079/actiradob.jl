# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function cyclesDection()
  answer = ""
  open("DATA.lst") do f
    ntestCases = parse(Int32,readline(f))
    answer = ""
    for i = 1:ntestCases
      dataTest = split(readline(f))
      total = parse(Int64,dataTest[1])
      testCity = dataTest[2][1]
      comparationString = ""
      lack = total
      optimize = 0
      while lack > 0 && optimize == 0
        for j = 2:total+1
          if in(testCity,dataTest[j])
            if dataTest[j][1] == testCity
              if in(dataTest[j][3],comparationString)
                optimize = 1
              else
                comparationString = string(comparationString,dataTest[j][3])
              end
            else
              if in(dataTest[j][1],comparationString)
                optimize = 1
              else
                comparationString = string(comparationString,dataTest[j][1])
              end
            end
            lack = lack - 1
            dataTest[j] = ""
          end
        end
        testCity = comparationString[1]
        comparationString = comparationString[2:length(comparationString)]
      end
      answer = string(answer," ",optimize)
    end
  end
  answer = answer[2:length(answer)]
  return answer
end

cyclesDection()

# $ include("actiradob.jl")
# "0 1 0 0 0 1 1 0 0 1 0 1 1 0 1 0 1 0 1 0 0 1 1 0 0 1 1 1 1"
