let out = ref "";;
print_string "ingrese el numero de pruebas: \n";;
let c = ref (read_int());;
let intArray = Array.make 5 0;;
let operations y = 
    let x = ref y in
      let sigu = ref true in
        for j=0 to 4 do
          if((intArray.(j)<>0)&& !sigu)then(
            let flag = ref false in
              for k = (j+1) to 4 do
                if(intArray.(j) = intArray.(k))then(          
                  x := !x + 1;                  
                  intArray.(k)<-0;
                  flag := true;
                );
                if((!x>1) && (k = 4))then(sigu := false);
              done;
              if((!x > 1)&&(!flag))then(
                intArray.(j)<-0;      
              );
          )
        done;
    !x;
  ;;
while !c>0 do
  print_string "ingrese cinco numeros separados por un espacio: \n";
  let data = read_line() in
  let intList = List.map int_of_string(Str.split (Str.regexp " ") data) in
  (*let intArray = Array.make 5 0 in*)
  let t = Array.make 5 0 in
  for i=0 to 4 do
    intArray.(i) <- (List.nth intList i);
    t.(i) <- (List.nth intList i);
  done;
  let c1 = ref 1 in
  let c2 = ref 1 in  
  c1 := (operations !c1);
  c2 := (operations !c2);
  if(!c1=2 && !c2=1)then(out:= !out^"pair ");
  if(!c1=3 && !c2=1)then(out:= !out^"three ");
  if(!c1=4 && !c2=1)then(out:= !out^"four ");
  if(!c1=2 && !c2=2)then(out:= !out^"two-pairs ");
  if(!c1=5)then(out:= !out^"yacht ");
  if((!c1=2 && !c2=3)||(!c1=3 && !c2=2))then(out:= !out^"full-house ");
  if(!c1=1)then(
      let f = ref 0 in
      for k=0 to 4 do
       if(!f=0)then(
        if(t.(k)=6)then(
          out:= !out^"big-straight ";
          f := 1;
        )
       ); 
      done;
      if(!f=0)then(out:= !out^"small-straight ");
  );  
  c := !c - 1;
done;;
Printf.printf "%s "!out;
print_string "\n";;
