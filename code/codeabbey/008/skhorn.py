#!/usr/bin/env python3
"""
Problem #8 Arithmetic Progression
"""
class ArithmeticProgression:
    """
    Arithmetic progression according to the following rule
    A + (A + B) + (A + 2B) + (A + 3B) + ...
    """
    total = []

    def __init__(self):
        while True:

            line = input()
            if line:
                if len(line) < 3:
                    pass
                else:
                    values = line.split()
                    self.progression(*values)
            else:
                break

        text = ' '.join(self.total)
        print(text)

    def progression(self, *args):
        """
        Fn to deal which such arithmetic progression
        appendind at the end to the global variable the sequence
        Args:
            *args   (qarg): data

        """
        sequence = 0
        first_value = int(args[0])
        step_size = int(args[1])
        values_accounted = int(args[2])

        for i in range(values_accounted):
            sequence += first_value + (step_size * i)

        print("First Value:{0}, Step Size: {1},Values accounted: {2}"\
              .format(first_value, step_size, values_accounted))
        self.total.append(str(sequence))


ArithmeticProgression()
