using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Número de repeticiones");
            int n = int.Parse(Console.ReadLine());
            int valo = 0,incr=0,term=0,suma=0;
            for (int i=0; i<n;i++) {
                valo = int.Parse(Console.ReadLine());
                incr = int.Parse(Console.ReadLine());
                term = int.Parse(Console.ReadLine());
                suma = 0;
                for (int j=0;j<term ;j++) {
                    suma = suma + valo + (incr * j);
                }
                Console.WriteLine("La suma es: "+suma);
            }
            
        }
    }
}
