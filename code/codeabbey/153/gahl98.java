/*
$ pmd -d gahl98.java -R rulesets/java/quickstart.xml -f text -min 2 #linting
May 08, 2019 5:00:52 PM net.sourceforge.pmd.PMD processFiles
WARNING: This analysis could be faster, please consider using
Incremental Analysis:
https://pmd.github.io/pmd-6.13.0/pmd_userdocs_incremental_analysis.html
$ javac -d . gahl98.java #Compilation
*/

package training.fluid;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

class Fermat{
  public static BigInteger bigIntSqRootFloor(BigInteger x)
          throws IllegalArgumentException {
      if (x.compareTo(BigInteger.ZERO) < 0) {
          throw new IllegalArgumentException("Negative argument.");
      }
      // square roots of 0 and 1 are trivial and
      // y == 0 will cause a divide-by-zero exception
      if (x .equals(BigInteger.ZERO) || x.equals(BigInteger.ONE)) {
          return x;
      } // end if
      BigInteger two = BigInteger.valueOf(2L);
      BigInteger y;
      // starting with y = x / 2 avoids magnitude issues with x squared
      for (y = x.divide(two);
              y.compareTo(x.divide(y)) > 0;
              y = ((x.divide(y)).add(y)).divide(two));
      return y;
  } // end bigIntSqRootFloor

  public static BigInteger bigIntSqRootCeil(BigInteger x)
          throws IllegalArgumentException {
      if (x.compareTo(BigInteger.ZERO) < 0) {
          throw new IllegalArgumentException("Negative argument.");
      }
      // square roots of 0 and 1 are trivial and
      // y == 0 will cause a divide-by-zero exception
      if (x == BigInteger.ZERO || x == BigInteger.ONE) {
          return x;
      } // end if
      BigInteger two = BigInteger.valueOf(2L);
      BigInteger y;
      // starting with y = x / 2 avoids magnitude issues with x squared
      for (y = x.divide(two);
              y.compareTo(x.divide(y)) > 0;
              y = ((x.divide(y)).add(y)).divide(two));
      if (x.compareTo(y.multiply(y)) == 0) {
          return y;
      } else {
          return y.add(BigInteger.ONE);
      }
  }

  public static void main(String[] args) {
    String message;
    BigInteger qoot = BigInteger.ZERO;
    BigInteger p, q, phi, d, a, n, cypher;
    BigInteger e = BigInteger.valueOf(65537);
    File file = null;
    Scanner input = null;
    ArrayList<BigInteger> finding = null;
    try {
      file = new File("DATA.lst");
      input = new Scanner(file);
      finding = new ArrayList<BigInteger>();
      while (input.hasNext()) {
        finding.add(input.nextBigInteger());
      }
    }
    catch (Exception error) {
      error.printStackTrace();
    }
    String sol="";
    n = finding.get(0);
    cypher = finding.get(1);
    BigInteger root =  bigIntSqRootCeil(n);
    root = root.subtract(BigInteger.ONE);
    do {
      root = root.add(BigInteger.ONE);
      qoot = root.pow(2).subtract(n);
    } while (!bigIntSqRootCeil(qoot).
    equals(bigIntSqRootFloor(qoot)));
    p = root.subtract(bigIntSqRootFloor(qoot));
    q = root.add(bigIntSqRootFloor(qoot));
    phi = n.subtract(p).subtract(q).add(BigInteger.ONE);
    d = e.modInverse(phi);
    a = cypher.modPow(d, n);
    message = a.toString();
    message = message.split("00")[0];
    for(int i=0; i<message.length()-1; i+=2){
      System.out.print((char)Integer.
      parseInt(message.substring(i, i+2)));
    }
    System.out.print("\n");
  }
}
/*
$ java training.fluid.Fermat
EON COW RAN BOY FAN SHY SKY AXE CAR
*/
