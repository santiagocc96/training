"""
Pawn Move Validator, coddeabey #76

pylint podany270895.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""


# pylint: disable=C0321
def create_chess_table():
    """
    Sets the chess table with all its pieces and positions
    """
    table = {}
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    for i in xrange(1, 9):
        for j in letters:
            table[j + str(i)] = ' '
    table['a8'] = 'r'; table['a7'] = 'p'  # noqa: E702
    table['a2'] = 'P'; table['a1'] = 'R'  # noqa: E702
    table['b8'] = 'n'; table['b7'] = 'p'  # noqa: E702
    table['b2'] = 'P'; table['b1'] = 'N'  # noqa: E702
    table['c8'] = 'b'; table['c7'] = 'p'  # noqa: E702
    table['c2'] = 'P'; table['c1'] = 'B'  # noqa: E702
    table['d8'] = 'q'; table['d7'] = 'p'  # noqa: E702
    table['d2'] = 'P'; table['d1'] = 'Q'  # noqa: E702
    table['e8'] = 'k'; table['e7'] = 'p'  # noqa: E702
    table['e2'] = 'P'; table['e1'] = 'K'  # noqa: E702
    table['f8'] = 'b'; table['f7'] = 'p'  # noqa: E702
    table['f2'] = 'P'; table['f1'] = 'B'  # noqa: E702
    table['g8'] = 'n'; table['g7'] = 'p'  # noqa: E702
    table['g2'] = 'P'; table['g1'] = 'N'  # noqa: E702
    table['h8'] = 'r'; table['h7'] = 'p'  # noqa: E702
    table['h2'] = 'P'; table['h1'] = 'R'  # noqa: E702
    return table


# pylint: disable=R0911
def evaluate_movement(first_pos, final_pos, table):
    """
    Given a table, this function calculates if a pawn movement is valid.
    If the piece is not a pawn, it just moves it whiout validating.
    """
    is_white_pawn = table[first_pos] == 'P'
    is_black_pawn = table[first_pos] == 'p'
    if is_white_pawn:
        l_index = first_pos[0]
        n_index_1 = str(int(first_pos[1])+1)
        n_index_2 = str(int(first_pos[1])+2)

        one_step_ahead = (final_pos == l_index+n_index_1)

        two_steps_ahead = (final_pos == l_index+n_index_2
                           and table[l_index+n_index_1] == ' '
                           and first_pos[1] == '2')

        attack_left = (final_pos == chr(ord(l_index) - 1)+n_index_1
                       and table[final_pos].islower())

        attack_right = (final_pos == chr(ord(l_index) + 1)+n_index_1
                        and table[final_pos].islower())

        if (one_step_ahead or two_steps_ahead) and table[final_pos] == ' ':
            table[final_pos] = table[first_pos]
            table[first_pos] = ' '
            return True
        elif ((attack_left or attack_right) and table[final_pos] != ' '):
            table[final_pos] = table[first_pos]
            table[first_pos] = ' '
            return True
        return False
    elif is_black_pawn:
        l_index = first_pos[0]
        n_index_1 = str(int(first_pos[1])-1)
        n_index_2 = str(int(first_pos[1])-2)

        one_step_ahead = (final_pos == l_index+n_index_1)

        two_steps_ahead = (final_pos == l_index+n_index_2
                           and table[l_index+n_index_1] == ' '
                           and first_pos[1] == '7')
        attack_left = (final_pos == chr(ord(l_index) - 1)+n_index_1
                       and table[final_pos].isupper())

        attack_right = (final_pos == chr(ord(l_index) + 1)+n_index_1
                        and table[final_pos].isupper())

        if (one_step_ahead or two_steps_ahead) and table[final_pos] == ' ':
            table[final_pos] = table[first_pos]
            table[first_pos] = ' '
            return True
        elif ((attack_left or attack_right) and table[final_pos] != ' '):
            table[final_pos] = table[first_pos]
            table[first_pos] = ' '
            return True
        return False
    else:
        table[final_pos] = table[first_pos]
        table[first_pos] = ' '
        return True


def who_is_moving(first_pos, table):
    """
    True if white pieces are moving
    False if black pieces are moving
    """
    if table[first_pos].isupper():
        return True
    return False


def read_input_file(path):
    """
    Parses input file and generates a matrix with the evaluations
    """
    input_file = open(path).read().splitlines()
    evaluations_number = int(input_file[0])
    evaluations = []
    for i in xrange(1, evaluations_number+1):
        evaluation = input_file[i]
        evaluations.append(evaluation)
    evaluations = [x.split() for x in evaluations]
    return evaluations


def main():
    """
    Loops through the evaluations and makes sure to print 0 if all movements
    are correct or the index of the incorrect movent caused by whether an
    invalid movement of a piece or a double white/black turn
    """
    evaluations = read_input_file("DATA.lst")
    answer = ""
    for evaluation in evaluations:
        table = create_chess_table()
        error_found = False
        last_move = False
        for i in xrange(len(evaluation)):
            first_pos = evaluation[i][0:2]
            last_pos = evaluation[i][2:4]
            current_move = who_is_moving(first_pos, table)
            last_eval = evaluate_movement(first_pos, last_pos, table)
            if(not last_eval or last_move == current_move):
                answer = answer + str(i + 1) + ' '
                error_found = True
                break
            last_move = current_move
        if not error_found:
            answer = answer + '0 '
    print answer


main()

# pylint: disable=pointless-string-statement

"""
python podany270895.py
output:
6 4 0 0 6 0 6 4 4 3 4 4 2
"""
