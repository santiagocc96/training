#include <iostream>
#include <string>

using namespace std;

void read_vals(int num,long *perm){
    int i;
    for(i=0;i<num;i++){
        cin>>*(perm+i);
    }
}

void FNS(long perm,char *fns_num){
    int i=1;
    while(perm/i!=0){
        *(fns_num-i+1)=(char)(perm%i)+48;
        perm/=i;
        i++;
    }
    *(fns_num-i+1)=(char)(perm%i)+48;
}

void next_perm(int num,long *perm){
    int i;
    for(i=0;i<num;i++){
        string base="ABCDEFGHIJKL",fns_num="000000000000";
        int j;
        FNS(*(perm+i),&fns_num[fns_num.length()-1]);    
        for(j=0;j<fns_num.length();j++){
            cout<<base[(int)fns_num[j]-48];
            base.erase((int)fns_num[j]-48,1);
        }
    cout<<" ";
    }
}

int main(void){
    int num;
    cin>>num;
    long int perm[num];
    read_vals(num, &perm[0]);
    next_perm(num,&perm[0]);
    return 0;
}
