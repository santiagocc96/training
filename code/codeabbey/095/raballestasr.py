#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Codeabbey 95: Simple linear regression
"""
def regression(expl, indep):
    """ Computes the slope and intercept of a simple
    linear regression line
    expl are the values of the EXPLanatory variable (x)
    indep are the values of the INDEPendent variable
    """
    obs = len(expl)
    sumx, sumxy, sumx2, sumy = 0, 0, 0, 0
    for i in range(obs):
        sumx += expl[i]
        sumy += indep[i]
        sumx2 += expl[i]**2
        sumxy += expl[i]*indep[i]
    slope = (obs*sumxy - sumx*sumy)/(obs*sumx2 - sumx**2)
    inter = (sumy - slope*sumx)/obs
    return str(slope) + " " + str(inter)

def main():
    """ Read codeabbey usual test cases
    """
    expl, indep = [], []
    data = open("DATA.lst", "r")
    ini, fin = list(map(int, data.readline().split()))
    for _i in range(fin-ini+1):
        line = data.readline().split()
        print(str(line))
        expl.append(int(line[1]))
        indep.append(int(line[2]))
    print(regression(expl, indep))
    data.close()

main()

#[rab@vaio codeabbey]$ pylint raballestasr.py
#No config file found, using default configuration
#************* Module regr
#C: 45, 0: Final newline missing (missing-final-newline)
#
#------------------------------------------------------------------
#Your code has been rated at 9.58/10 (previous run: 5.00/10, +4.58)
