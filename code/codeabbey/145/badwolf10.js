/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */

/* Decimal precision library is necesary for big number multiplication,
normal multiplication is inaccurate*/
const Decimal = require('decimal.js');

function modexp(aaa, bbb, ccc) {
  const aval = new Decimal(aaa);
  const bval = new Decimal(bbb);
  const cval = new Decimal(ccc);
  if (bval.eq(0)) {
    return 1;
  } else if (bval.mod(2).eq(0)) {
    const dval = new Decimal(modexp(aval.toFixed(0), bval.div(2).toFixed(0),
      cval.toFixed(0)));
    return dval.mul(dval).mod(cval).toFixed(0);
  }
  const mexp = new Decimal(modexp(aval.toFixed(0), bval.toFixed(0) - 1,
    cval.toFixed(0)));
  return mexp.mul(aval.mod(cval)).mod(cval).toFixed(0);
}

function getModExp(triplets) {
  return modexp(triplets[0], triplets[1], triplets[2]);
}
// some functions do not need to return a value
/* eslint-disable fp/no-unused-expression*/
function findGcd(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n').slice(1);
  const ntriplets = dataLines.map((abc) => abc.split(' ').map(Number));
  const modexplist = ntriplets.map(getModExp);
  // eslint-disable-next-line no-console
  modexplist.map((x) => console.log(x));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findGcd(readerr, contents));
}

main();
/*
$ node badwolf10.js
5022695
292780914
140818938
*/
