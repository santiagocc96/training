let a = Scanf.scanf "%d\n"(fun a ->  a);;

let printArray r = Array.iter (Printf.printf "%d ") r;;
let criba arr lim = arr.(0) <- false;
                    arr.(1) <- false;
                    let i = ref 2 in
                    let h = ref 2 in
                    while (!i* !i) <= lim do
                        if arr.(!i) = true then
                        begin
                            h := 2;
                            while (!i * !h) <= lim do
                                arr.(!i * !h) <- false;
                                h := !h + 1;
                            done;
                        end;
                        i := !i + 1;
                    done;;

let rec binary_search a value low high = if high = low then
                                            if a.(low) = value then
                                                low
                                            else
                                                raise Not_found
                                        else let mid = (low + high) / 2 in
                                            if a.(mid) > value then
                                                binary_search a value low (mid - 1)
                                            else if a.(mid) < value then
                                                binary_search a value (mid + 1) high
                                            else
                                                mid;;

let arr = Array.make 3000001 true;;
let primos = Array.make 216817 0;;
criba arr 3000000;;
let cont = ref 0;;
for i = 0 to 3000000 do
    if arr.(i) = true then
    begin
        primos.(!cont) <- i;
        cont := !cont + 1;
    end;
done;;

for i = 0 to a-1 do
    let limInf = Scanf.scanf "%d "(fun limInf ->  limInf) in
    let limSup = Scanf.scanf "%d\n"(fun limSup ->  limSup) in
    let indexInf = binary_search primos limInf 0 (Array.length primos - 1) in
    let indexSup = binary_search primos limSup 0 (Array.length primos - 1) in
    let result = indexSup - indexInf + 1 in
    Printf.printf "%d " result;
done;;
