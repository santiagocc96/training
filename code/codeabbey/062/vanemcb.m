%checkcode('vanemcb.m')

fileID = fopen('DATA.lst');
vecNum = fscanf(fileID,'%f %f',[2 inf]);
vecNum = vecNum';
cellPrimes = cell(length(vecNum),1);
vecFinal = zeros(1,length(vecNum));

for i=1:length(vecNum);
  P = primes(vecNum(i,2));
  in = find(P==vecNum(i,1));
  cellPrimes{i,1} = P(1,in:end);
  vecFinal(1,i) = length(cellPrimes{i,1});
end

disp(vecFinal);

%vanemcb
%83240 8887 14177 7452 51121 4484 11804 31344 2119 60923
%17235 27382 11202 38837 28516 43885 25869 5028 49326 9581
%32738 3878 16131 535 54619 3369 5562
