#include <iostream>
#include <map>
#include <string>

using namespace std;

string RWG(long int *X0){
    long int Xcurr=*X0;
  int A=445;
    long int C=700001,M=2097152;
    char cons[20]="bcdfghjklmnprstvwxz", vow[6]="aeiou";
    string word;
    for(int j=0;j<6;j++){
        long int X1;
        X1=(A*Xcurr+C)%M;
        Xcurr=X1;
        if(j%2==1){
            word+=vow[X1%5];
        }
        else{
            word+=cons[X1%19];
        }
        if(j==5){
          *X0=X1;
          return word;
        }
    }
    return word;
}

void create_words(long int X0){
  long int max=0;
  string mfw;
  map<string,int> word_list;
  for(int i=0;i<900000;i++){
      string word;
      hash<string> word_hash;
      long int n;
      word=RWG(&X0);
      if(word_list.count(word)==1){
          word_list[word]+=1;
          n=word_list[word];
      }
      else{
        word_list.insert(pair<string,int>(word,1));
      }
      if(n>max){
          max=n;
          mfw=word;
      }
  }
  cout<<mfw;
}

int main(void){
    long int X0;
    cin>>X0;
    create_words(X0);
    return 0;
}
