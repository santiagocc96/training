/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression*/

const base = 3;
const tdtermn = -1;

function findTernaryDecomposition(mass, desc) {
  if (mass === 1) {
    return desc.concat(1);
  }
  const tdterm = (mass % base) === 2 ? tdtermn : mass % base;
  return findTernaryDecomposition((mass - tdterm) / base, desc.concat(tdterm));
}

function findMassBalanceNumber(mass) {
  const tdesc = findTernaryDecomposition(mass, []);
  return tdesc.filter((tdterm) => tdterm !== 0).length;
}

function findMasses(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const masses = dataLines[1].split(' ').map(Number);
  const nmasses = masses.map(findMassBalanceNumber);
  // eslint-disable-next-line no-console
  nmasses.map((nmass) => console.log(nmass));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findMasses(readerr, contents));
}

main();

/*
$ node badwolf10.js
9 1 11 8 7 6 7 6 12 7 3 8 10 14 12 8 6 8 4 5
*/
