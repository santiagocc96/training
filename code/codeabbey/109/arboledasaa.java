
/*
$ javac arboledasaa.java
0
$
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringJoiner;

//due to compilation and posting requirements,
//class naming conventions are omitted
public class arboledasaa {

  public static void main(String[] args)
      throws NumberFormatException, IOException {

    arboledasaa j = new arboledasaa();
    j.work();

  }

  private void work() throws NumberFormatException, IOException {
    // pre
    int numberOfTestCases, n, b;
    /*
     * the input stream
     */
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    /*
     * a temporal variables
     */
    ArrayList<Long> totals = new ArrayList<>();
    String array[];
    // input
    /*
     * Input data contains the number of test-cases in the first line.
     *
     * Next lines contain a pair of values N and B each.
     */
    numberOfTestCases = Integer.parseInt(in.readLine());
    for (int i = 0; i < numberOfTestCases; i++) {
      // due to input format this never happens
      // Check for oddness that won't work for negative numbers in
      // arboledasaa.work() [Troubling(13), Normal confidence]
      array = in.readLine().split(" ");
      n = Integer.parseInt(array[0]);
      b = Integer.parseInt(array[1]);
      int nPrime;
      // due to input format this never happens
      // Dereference of the result of readLine() without a null check in
      // arboledasaa.work() [Of Concern(15), Normal confidence]
      if (n % 2 == 1) {
        nPrime = (n - 1) / 2;
      } else {
        nPrime = n / 2;
      }

      long[] matrix = new long[nPrime * b];
      // processing
      long total = 0;
      TicketGenerator generator = new TicketGenerator(nPrime, b);
      String ticket = generator.getNext();
      while (ticket != null) {
        int sum = sum2(ticket);
        matrix[sum] = matrix[sum] + 1;
        ticket = generator.getNext();
      }
      for (int j = 0; j < matrix.length; j++) {
        total += matrix[j] * matrix[j];
      }
      if (n % 2 == 1) {
        total *= b;
      }
      if (n == 1) {
        total = b;
      }
      totals.add(total);
    }
    // output
    output(totals);
    // post
    in.close();
    System.exit(0);
  }

  public static int sum(String string) {
    String conversionTable = "0123456789abcdef";
    int answer = 0;
    char[] chars = string.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      answer += conversionTable.indexOf(chars[i]);
    }
    return answer;
  }

  public static int sum2(String string) {
    int answer = 0;
    char[] chars = string.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      answer += Integer.parseInt(chars[i] + "", 16);
    }
    return answer;
  }

  private static void output(ArrayList<Long> totals) {
    StringJoiner sj = new StringJoiner(" ");
    for (Object element : totals) {
      Long total = (Long) element;
      sj = sj.add(total.toString());
    }
    // according to PMD this shouldn't be used SystemPrintln in production, but
    // for this exercice it should be fine
    System.out.println(sj.toString());
  }

}

class TicketGenerator {

  private int n, b;
  private int current;
  private String format;

  public TicketGenerator(int n, int b) {
    this.n = n;
    this.b = b;
    current = 0;
    for (int i = 0; i < n; i++) {
      format += "0";
    }
  }

  public String getNext() {
    String result = Integer.toString(current, b);
    if (result.length() <= n) {
      current++;
      result = format + result;
      int length = result.length();
      return result.substring(length - n, length);
    } else {
      return null;
    }
  }

}

/*
 * $ cat DATA.lst | java arboledasaa
 *
 * 25288120 184756 128912240 344 252 22899818176 58199208 40 8 8092 1231099425
 * 1132138107 44152809
 */
