'''
$ pylint develalopez.py
No config file found, using default configuration

-------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.71/10, +0.29)
$ flake8 develalopez.py
'''


from __future__ import print_function


def combinations(to_process, processed, top_val, num_values):
    '''
    Function that returns the number of different combinations that can be
    added up to each number from 0 to the size of the values list that
    is given.
    '''
    if to_process == 0:
        for i in range(top_val):
            new_processed = processed + i
            num_values[new_processed] += 1
    else:
        for i in range(top_val):
            combinations(to_process - 1, processed + i, top_val, num_values)


def get_tickets(digits, base):
    '''
    Function that gets a pair of numbers (digits nad base) and returns the
    number of lucky tickets that exist.
    '''
    ans = []
    extra = digits % 2 == 1
    if extra:
        digits -= 1
    max_value = int((digits / 2) * (base - 1))
    count = 0

    # If ticket is one digit long return base, else calculate combinations.
    if digits > 0:
        num_values = [0] * (max_value + 1)
        combinations(digits / 2 - 1, 0, base, num_values)
        for i in num_values:
            count += i ** 2

        # If digit was removed at beggining, recover it.
        if extra:
            ans = count * base
        else:
            ans = count

    else:
        ans = base

    return ans


def main():
    '''
    Function that returns the number of luckty tickets for each given pair of
    digits and bases given.
    '''
    data_file = open("DATA.lst", "r")
    cases = int(data_file.readline())
    ans = []
    for _ in range(cases):
        digits, base = map(int, [int(x) for x in data_file.readline().split()])
        ans.append(get_tickets(digits, base))

    print(" ".join(str(x) for x in ans))


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
25288120 184756 128912240 344 252 22899818176 58199208 40 8 8092 1231099425
1132138107 44152809
'''
