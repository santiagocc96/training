let t = Array.make 10 "p";;
let validate elem po =    
    match po with     
     1 -> if( (t.(2) = elem && t.(3) = elem) || (t.(4) = elem && t.(7) = elem) || (t.(5) = elem && t.(9) = elem) ) then false else true
    |2 -> if( (t.(1) = elem && t.(3) = elem) || (t.(5) = elem && t.(8) = elem) ) then false else true
    |3 -> if( (t.(1) = elem && t.(2) = elem) || (t.(6) = elem && t.(9) = elem) || (t.(5) = elem && t.(7) = elem) ) then false else true
    |4 -> if( (t.(1) = elem && t.(7) = elem) || (t.(5) = elem && t.(6) = elem) ) then false else true
    |5 -> if( (t.(1) = elem && t.(9) = elem) || (t.(3) = elem && t.(7) = elem) || (t.(2) = elem && t.(8) = elem) || (t.(4) = elem && t.(6) = elem) ) then false else true
    |6 -> if( (t.(3) = elem && t.(9) = elem) || (t.(4) = elem && t.(5) = elem) ) then false else true
    |7 -> if( (t.(1) = elem && t.(4) = elem) || (t.(3) = elem && t.(5) = elem) || (t.(8) = elem && t.(9) = elem) ) then false else true
    |8 -> if( (t.(2) = elem && t.(5) = elem) || (t.(7) = elem && t.(9) = elem) ) then false else true
    |9 -> if( (t.(7) = elem && t.(8) = elem) || (t.(1) = elem && t.(5) = elem) || (t.(3) = elem && t.(6) = elem) ) then false else true
    |_ -> false
;;
print_string "ingrese el numero de pruebas: \n";;
let pr = ref ( read_int () );;
let out = Array.make !pr (-1);;
let ind = ref 0;;
while !pr > 0 do
  Printf.printf "juego nuevo %i \n" (!ind + 1);
  let i = ref 1 in
    let counter = ref 0 in
      let still = ref true in        
          while !still do           
            if ((!i mod 2) <> 0) then (
                i := !i + 1;
                print_string "turno de X, ingrese la posición: \n";
                let p = read_int () in
                t.(p) <- "x";
                counter := !counter + 1;
                still := (validate "x" p);
              ) else (
                i := !i - 1;
                print_string "turno de O, ingrese la posición: \n";
                let p = read_int () in
                t.(p) <- "o";
                counter := !counter + 1;
                still := (validate "o" p);
            );
             if( !still = false) then (
                out.(!ind) <- !counter;                
                for m = 0 to 9 do
                  t.(m)<-"p";
                done; 
             );
             if(!counter = 9) then (
                if(!still) then (
                  out.(!ind) <- 0;
                  still := false;
                ) else (out.(!ind) <- 9);
                for m = 0 to 9 do
                  t.(m)<-"p";
                done;
             );           
          done;    
  ind := !ind + 1;
  pr := !pr - 1;
done;;
for k = 0 to ((Array.length out) - 1) do
  if( k != ((Array.length out) - 1) ) then Printf.printf "%i " out.(k) else Printf.printf "%i \n" out.(k);
done;;
