/*
$ rustfmt santiyepes.rs --write-mode=diff
$
$ rustc santiyepes.rs
$
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let file = File::open("DATA.lst").unwrap();

  for buff in BufReader::new(file).lines() {
    let mut line: &str = &buff.unwrap();
    let mut array: Vec<&str> = line.split(' ').collect();
    let mut numbers: Vec<i64> = Vec::new();

    for x in &array {
      let mut string_number: String = x.to_string();
      let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
      numbers.push(number_conver);
    }
    if numbers.len() > 1 {
      if numbers[0] > numbers[1] && numbers[0] < numbers[2] {
        print!("{} ", numbers[0]);
      }
      if numbers[0] < numbers[1] && numbers[0] > numbers[2] {
        print!("{} ", numbers[0]);
      }
      if numbers[1] > numbers[0] && numbers[1] < numbers[2] {
        print!("{} ", numbers[1]);
      }
      if numbers[1] < numbers[0] && numbers[1] > numbers[2] {
        print!("{} ", numbers[1]);
      }
      if numbers[2] > numbers[0] && numbers[2] < numbers[1] {
        print!("{} ", numbers[2]);
      }
      if numbers[2] < numbers[0] && numbers[2] > numbers[1] {
        print!("{} ", numbers[2]);
      }
    }
  }
}

/*
$ ./santiyepes
4 1087 22 57 503 80 161 811 620 112
952 52 15 847 13 90 38 36 308 14 971 117 250
284 11 929 151 358
*/
