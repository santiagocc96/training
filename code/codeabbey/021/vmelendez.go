/*
$ ./golint vmelendez.go
$ go run vmelendez.go
*/
package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {
  f, err := os.Open("DATA.lst")
  if err != nil {
    fmt.Printf("error opening file: %v\n", err)
    os.Exit(1)
  }

  var x [1000]string
  scanner := bufio.NewScanner(f)
  i := 0
  for scanner.Scan() {
    x[i] = scanner.Text()
    i++
  }

  s := strings.Split(x[0], " ")
  n, _ := strconv.Atoi(s[0])
  p, _ := strconv.Atoi(s[1])

  var num[1000]int
  for i := 0; i < n; i++ {
    r := strings.Split(x[1], " ")
    for j := 0; j < p; j++ {
      if (strconv.Itoa(j+1) == r[i]) {
        num[j]++;
      }
    }
  }

  for i := 0; i < p; i++ {
    fmt.Printf("%v ", num[i])
  }
}
/*
$ go run vmelendez
21 24 32 24 23 25 29 27 25 28 16 19 30
*/
