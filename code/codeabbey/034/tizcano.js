/*
$ eslint tizcano.js
$
*/

function add(valInput, left, right) {
  const divider = 50;
  const third = 3;
  const middle = (left + right) / 2;
  const aByX = valInput[0] * middle;
  const bByP = valInput[1] * Math.sqrt(Math.pow(middle, third));
  const cByE = valInput[2] * Math.exp(-middle / divider);
  const formula = aByX + bByP - cByE - valInput[third];
  const epsilon = 0.00000001;
  if (formula < -epsilon) {
    return add(valInput, middle, right);
  } else if (formula > epsilon) {
    return add(valInput, left, middle);
  }
  return formula;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const inputContents = inputFile.slice(1);
  const left = 0.0;
  const right = 100.0;
  const solvedObject = inputContents.map((element) =>
    add(element.split(' '), left, right)
  );
  const output = process.stdout.write(`${ solvedObject.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
-2.8887825465062633e-9 -8.112550631267368e-9 7.516973710153252e-10
-6.527102414111141e-9 -4.1950443119276315e-11 2.450406100251712e-9
4.860794433625415e-9
*/
