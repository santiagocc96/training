## Version 2.0
## language: en

Feature:  Web Server - Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    jdanilo7
  Goal:
    Get the flag exploiting php's loose comparison operator

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |      65.0.2     |
  Machine information:
    Given The challenge url
    """
    http://challenge01.root-me.org/web-serveur/ch55/index.php
    """
    And The challenge statement
    """
    Find a way to get the flag.
    No bruteforce needed.
    """
    Then I am asked to get a flag exploiting php's loose comparison

  Scenario: Success: Find a string whose md5 looks like 0eXXX
    Given I click the "Start the challenge" button
    Then I am redirected to a page with two text fields, a button and a link
    And one is a field for a seed string and the other one for a hash
    And the link states "source code"
    Given I click the "source code" linke
    Then the code that processes the seed and the hash is shown
    """
    14  function gen_secured_random() { // cause random is the way
    15    $a = rand(1337,2600)*42;
    16    $b = rand(1879,1955)*42;
    17    $a < $b ? $a ^= $b ^= $a ^= $b : $a = $b;
    18    return $a+$b;
    19  }
    20
    21  function secured_hash_function($plain) { // cause md5 is the best hash
                                                    ever
    22    $secured_plain = sanitize_user_input($plain);
    23    return md5($secured_plain);
    24  }
    25  function sanitize_user_input($input) { // cause someone told me to
                                                  never trust user input
    26    $re = '/[^a-zA-Z0-9]/';
    27    $secured_input = preg_replace($re, "", $input);
    28    return $secured_input;
    29  }
    30  if (isset($_POST['s']) && isset($_POST['h'])) {
    31    $s = sanitize_user_input($_POST['s']);
    32    $h = secured_hash_function($_POST['h']);
    33    $r = gen_secured_random();
    34    if($s != false && $h != false) {
    35      if($s.$r == $h) {
    36        print "Well done! Here is your flag: ".$flag;
    37      }
    38      else {
    39        print "Fail...";
    40      }
    41    }
    42    else {
    43    print "<p>Hum ...</p>";
    44    }
    45  }
    """
    And I notice the comparison in line 35 is loose
    And it can be exploited if I enter a string that is md5 encoded like 0eXX
    Given I try to decrypt an md5 hash in the format 0eXX using this link
    """
    https://www.md5online.org/md5-decrypt.html
    """
    Then I find that 0e222111582876893454833329981234 md5-encodes 18798071254
    Given I enter 0e123 in the seed field and 18798071254 in the hash field
    And I click the "Check" button
    Then I get this success message
    """
    Well done! Here is your flag: F34R_Th3_L0o5e_C0mP4r15On
    """
    And I validate the flag on the challenge website
    And it is accepted
