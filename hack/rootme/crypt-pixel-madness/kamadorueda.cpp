/*
 * $ cppcheck    kamadoatfluid.cpp
 * $ g++      -o kamadoatfluid     ./kamadoatfluid.cpp
 */

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
  ifstream file("./pixels.txt");

  for (string token;;) {
    file >> token;
    if (token == "eof") break;
    if (token == "eol") cout << endl;
    else {
      char c;
      int i = stoi(token.substr(2));
      if (token[0] == '0') c = ' ';
      else                 c = '#';

      while (i--) cout << c;
    }
  }
}

/*
 * $ ./kamadoatfluid
 *     ... somehow you can see "SOLUTION" on ASCII-ART ...
 */
