# lenguage: en

Feature: Solve network-ip-time-to-live
  From root-me site
  Category Network
  With my username synapkg

  Background:
    Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
    And I am using Mozilla Firefox Quantum 63.0 (64-bit)
    And I am using Wireshark 2.9.0

  Scenario: Successful attempt
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/IP-Time-To-Live
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    Find the TTL used to reach the targeted host in this ICMP exchange.
    """
    Then I started challenge downloading the capture from this link
    """
    http://challenge01.root-me.org/reseau/ch7/ch7.pcap
    """
    Then I opened the capture with wireshark
    And I investigated about time to live attack
    Then I come back to the capture
    And I discoreved one thing in the last part of capture
    And it is with ttl in "13" there no was a answer of time to live exceeded
    When source sent a packet to destination.
    And I put "13" without quotes as answer and I was right.
