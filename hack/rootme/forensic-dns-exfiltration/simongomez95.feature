## Version 2.0
## language: en

Feature: forensic-dns-exfiltration
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Get the exfiltrated data from captured traffic

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |

  Scenario: Success:traffic-analysis
    Given I download the pcap file from  the challenge
    And open it with Wireshark
    Then I see a lot of DNS requests and responses
    And notice the requests are being made to really long addresses
    And those adresses look like hex data ([0-9a-fA-F])
    And they all end with ".jz-n-bs.local"
    And some of them are CNAME, some MX and some TXT
    Then I write a python script to isolate the data payloads
    """
    b'e\xbe\x01$\x10\x15\xba\xe3c'
    b'\x01\x9d\x01$\x10\x15\xba\xe3c'
    b'l\x06\x01$\x10\x15\xba\xe3t\x00\x00\x1b\x15\x80\x01\x00\x03\x89PNG\r\n\x1a
    \n\x00\x00\x00\rIHDR\x00\x00\x02\x80\x00\x00\x01\xe0\x08\x06\x00\x00\x005\xd
    1\xdc\xe4\x00\x00\x00\x06bKGD\x00\xff\x00\xff\x00\xff\xa0\xbd\xa7\x93\x00\x0
    0\x00\tpHYs\x00\x00\x0b\x13\x00\x00\x0b\x13\x01\x00\x9a\x9c\x18\x00\x00\x00\
    x07tIME\x07\xe1\x06\x1b\x088\x06\xe9{\x0f\xcf'
    b'XL\x01$\x10\x16\x1d\xe3t\x00\x00\x00\x19tEXtComment\x00Created with GIMPW\
    x81\x0e\x17\x00\x00\x1ayIDATx\xda\xed\xdd{pT\xe5\xe1\xf8\xe17@@\x92\x00J\x04
    \x04\xa4Q[\x15\x15DP\x04I\xb5\xc5v*\xce\x04-T\xc6\xb6v\x1c\x91Nq\xda*Z,\xb6\
    xa3\xd5Z;\xdaa'
    ...
    b'\t\xb5\x01$\x100i\xe3t\x80\x00\x04\x00@\x00\x02\x00 \x00\x01\x00\x10\x80\x
    00\x00\x08@\x00\x00\x04 \x00\x80\x00\x04\x00@\x00\x02\x00 \x00\x01\x00\x10\x
    80\x00\x00\x08@\x00\x00\x04 \x00\x00\x02\x10\x00\x00\x01\x08\x00\x80\x00\x04
    \x00@\x00\x02\x00 \x00\x01\x00\x10\x80\x00\x00\x08@\x00\x00\x04 \x00\x80\x00
    \x04\x00\xe0`\xf0\xff\x00I1\x17/\x17\xc2Z\x18\x00\x00\x00\x00I'
    b'b\xc1\x01$\x100\xcc\xe3tEND\xaeB`\x82'
    b'4s\x01$\x100\xd3\xe3t'
    b'\x17\x11\x01$\x100\xd3\xe3t'
    b'8\xd8\x01$\x100\xd3\xe3t'
    b'{2\x01$\x100\xd3\xe3t'
    b'J\n\x01$\x100\xd3\xe3t'
    """
    Then I notice some chunks that give away there's a PNG image in there
    """
    x89PNG
    IHDR
    EXtComment\x00Created with GIMP
    IEND
    """
    Given some of the DNS requests are CNAME, some MX and some TXT
    And DNScat is the most popular DNS tunneling tool that uses all those types
    And the first 9 bytes of each DNScat request is DNScat metadata
    Then I strip the first 9 bytes of each request
    And remove all data before the PNG magic byte
    And remove all data after IEND
    Then I pipe my script's output to a png file
    Then I open the image and get the flag
