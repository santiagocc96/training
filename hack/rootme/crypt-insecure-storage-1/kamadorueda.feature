## Version 2.0
## language: en

Feature: Root Me - Crypto - File - Insecure storage 1
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    File - Insecure storage 1
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    |  Google Chrome  |  70.0.3538.77 |
    |    GNU bash     |     4.4.19    |
    |     Ubuntu      |   18.04.1LTS  |

  Machine information:
    Given I'm accesing the challenge page
    And the tittle is "File - Insecure storage 1"
    And the subtitle is "Mozilla Firefox 14"
    And the statement is "Retrieve the user’s password."
    And a file called "ch20.tgz" is provided

  Scenario: Success: Reconaissance of the .tgz file
    Given I extract the provided file
    """
    $ tar -xf ch20.tgz
    """
    And I list the directory structure
    """
    $ tree ./.mozilla/
    ./.mozilla/
    ├── extensions
    └── firefox
        ├── o0s0xxhl.default
        │   ├── bookmarkbackups
        │   │   └── bookmarks-2012-02-06.json
        │   ├── Cache
        │   │   ├── 0
        │   │   ├── 1
        │   │   ├── 2
        │   │   ├── 3
        │   │   ├── 4
        │   │   │   ├── 2F
        │   │   │   │   └── D09D1d01
        │   │   │   └── B1
        │   │   │       └── 8A3CFd01
        │   │   ├── 5
        │   │   │   └── 04
        │   │   │       └── 62401d01
        │   │   ├── 6
        │   │   │   └── 2E
        │   │   │       └── A07FCd01
        │   │   ├── 7
        │   │   ├── 8
        │   │   ├── 9
        │   │   ├── A
        │   │   ├── B
        │   │   ├── C
        │   │   ├── _CACHE_001_
        │   │   ├── _CACHE_002_
        │   │   ├── _CACHE_003_
        │   │   ├── _CACHE_MAP_
        │   │   ├── D
        │   │   ├── E
        │   │   └── F
        │   ├── cert8.db
        │   ├── chromeappsstore.sqlite
        │   ├── compatibility.ini
        │   ├── content-prefs.sqlite
        │   ├── cookies.sqlite
        │   ├── downloads.sqlite
        │   ├── extensions.ini
        │   ├── extensions.sqlite
        │   ├── formhistory.sqlite
        │   ├── key3.db
        │   ├── localstore.rdf
        │   ├── mimeTypes.rdf
        │   ├── OfflineCache
        │   │   └── index.sqlite
        │   ├── permissions.sqlite
        │   ├── places.sqlite
        │   ├── pluginreg.dat
        │   ├── prefs.js
        │   ├── search.json
        │   ├── search.sqlite
        │   ├── secmod.db
        │   ├── sessionstore.js
        │   ├── signons.sqlite
        │   ├── startupCache
        │   │   └── startupCache.8.little
        │   ├── urlclassifier3.sqlite
        │   ├── urlclassifierkey3.txt
        │   ├── urlclassifier.pset
        │   ├── weave
        │   │   ├── changes
        │   │   ├── failed
        │   │   └── toFetch
        │   └── webappsstore.sqlite
        └── profiles.ini
    """
    Then I see some interesting files "*.sqlite", "*.db", and "*.json"
    When I list this files only
    """
    # ls -aR ./.mozilla/ | grep -iE '(\.sqlite|\.db|\.json)'
    cert8.db
    chromeappsstore.sqlite
    content-prefs.sqlite
    cookies.sqlite           # useful!
    downloads.sqlite
    extensions.sqlite
    formhistory.sqlite
    key3.db                  # important!
    permissions.sqlite
    places.sqlite
    search.json
    search.sqlite
    secmod.db
    signons.sqlite           # important!
    urlclassifier3.sqlite
    webappsstore.sqlite
    """
    Then I notice as important two files "key3.db" and "signons.sqlite"

  Scenario: Success: get the database and decrypt the password
    When I inspect "signons.sqlite" with an online tool
    # http://inloop.github.io/sqlite-viewer/
    Then I get two tables
    When I look at the "moz_logins" table
    Then I see a username an a password in encrypted form
    """
    encryptedUsername
      MDoEEPgAAAAAAAAAAAAAAAAAAAEwFAYIKoZIhvcNAwcECGQiIGc9wcicBBDV2Zx+
      ouMBMu+QGgCAWJC8
    encryptedPassword
      MDoEEPgAAAAAAAAAAAAAAAAAAAEwFAYIKoZIhvcNAwcECL6IksL4y0rsBBCwsrL8
      AoQSAbNEoOvkOfbA
    """
    Given I do some research about the firefox encryption methods
    Then I found it is something related with the "NSS" project
    And I found a tool to decrypt this given a master password
    When I run the following command with empty master password
    # https://github.com/unode/firefox_decrypt
    """
    # python ./firefox_decrypt.py ./
    Master Password for profile ./:
    WARNING - Attempting decryption with no Master Password

    Website:   http://www.root-me.org
    Username: 'shell1cracked'
    Password: 'F1rstP4sSw0rD'
    """
    Then I see the decrypted password
    And I use "F1rstP4sSw0rD" as solution
    And I solve the challenge
