## Version 1.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    Cristian Guatuzmal
  Goal:
    Discover the decrypted text

    Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=2
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    For the beginning here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    4A3E374A4973483F3D3E4A
    """
    And an input format which allows me to cipher text
    And uses the same cipher pattern as the text

  Scenario: Success:method-trying-values-in-the-input
    Given the web page has an input field and an Encrypt Button
    Then I start to put some values in the input field
    And I start to encrypt them
    Then I found the encryptation pattern
    And it is an incremental code which goes like this
    """
    1[1,2,3,4,5,6], 2[a,b,c,d,e,f] 3[1,2,3,4,5,6]
    """
    And every position correlates with a character
    And I build a table with to decrypt that pattern
    And I decode the mesage which says(thats right)
    And I run it in the solution field
    Then I can see that the message is correct
    Then I conclude that my solution is the right decrypt pattern
    And I solved te challenge
