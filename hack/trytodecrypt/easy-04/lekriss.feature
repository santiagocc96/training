## Version 1.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    Cristian Guatuzmal
  Goal:
    Discover the decrypted text

  Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=4
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    For the beginning here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    0C02D8010D0C02D8010606D8101402FCD80F0603D8FC0600DA
    """
    And an input format which allows me to cipher text
    And uses the same cipher pattern as the text

  Scenario: Success:building the cipher pattern
    Given the web page has an input field and an Encrypt Button
    Then I start to put some values in the input field
    And I start to encrypt them
    Then I found the encryption pattern
    And it is an decremental code which goes like this
    """
    0 ([F-A{f-k},   [8-0]{m-u})
    1 ([4-0]{a-e})
    F ([F-A]{v-z-A},[9-0]{B-K})
    E ([F-A]{L-Q},  [9-0]{R-Z})
    D ([A]{?},      [9]{!})
    """
    And every position correlates with a character
    Then I use the cipher pattern to translate the crypted message
    And I decode the message which says(is this too easy for you?)
    And I run it in the solution field
    Then I can see that the message is correct
    Then I conclude that my solution is the right decrypt pattern
    And I solved te challenge
