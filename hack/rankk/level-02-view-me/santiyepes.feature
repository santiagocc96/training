# language: en

Feature: View me
  From site rankk.org
  From Level 2 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/view-me.py
  Message: Please login with your username and password.
  Objetive: Find the answer of the challenge
  Evidence: Script exposed in the source code
  """

Scenario: Login
The page shows two alerts asking for access credentials
  Then I close the alerts to see the source code
  And it is not allowed to see source code taking me out of the challenge
  Then I return to open the link of the challenge
  And I realize that the page does not finish loading
  Then I stop it while I close the alerts
  And finally I can see the source code
  Then I see that in a script you have the credentials
  """
  var username = "Genius";
  var password = "0o0o0o";
  var msg = "Enter your username";
  var un = prompt (msg,"");
  msg = "Enter password";
  var pw = prompt (msg,"");
  if (un == username && pw == password) {
  var url="view-me.py?solution=333966";
  window.open(url,"_self");
  }
  else {
  alert("Wrong!");
  window.location.href="/";
  }
  """
  Then I go back to the challenge page to enter the credentials
  And I solve the challenge
