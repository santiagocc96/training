# language: en

Feature: Wrong sum
  From site rankk.org
  From Level 1 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/wrong-sum.py
  Message: Judy's school is starting a Beginner's Javascript class. Judy signed
  up and after Day 1, she was all excited to get her hands dirty. This is her
  first attempt at summing an array of numbers. But it's not working...

  Help her fix the code and submit the modified line as the solution.
  Objetive: Find the answer of the challenge
  Evidence: Javascript function
  """

Scenario: Analyzing Javascript code
The page shows a statement with a Javascript function
  Then I analyze the Javascript code
  """
    function addup() {
    var total = "";
    var scores = new Array();
    scores = [50, 60, 70, 100, 80];
    for (var i = 0; i < scores.length; i++) {
      total += scores[i];
    }
    alert(total);
  }
  """
  And I find an error in a line of code
  """
  var total = "";
  """
  Then I correct the wrong code line
  """
  var total = 0;
  """
  Then he entered the corrected code line
  And I solve the challenge
