Feature: Solve Showtime challenge
  from site Rankk
  logged as Edprado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have Stegsolver program

Scenario: Challenge solved
  Given A GIF image with hieroglyphics
  When I download the image
  And I load it to Stegsolver
  When I freeze the first frame of the GIF
  And I google for "hieroglyphics meaning"
  Then I find a code in the hieroglyphics
  And I solve the challenge
