## Version 2.0
## language: en

Feature: caterpillar-WeChall
  Site:
    WeChall
  Category:
    Steganography
  User:
    dianaosorio97
  Goal:
    Find the hidden message in the image

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10 pro  | 10.0.17134  |
    | Google Chrome   | 70.0.353.110|
    | IllustratorCs6  | 16.0        |
  Machine information:
    Given I am accessing the challenge page
    And I see the problem statement
    """
    I have made a drawing of a Caterpillar and
    I have hidden some text in it. I am sure that
    you can solve it, since it is not very difficult
    """
    Then I download the image

  Scenario: Fail: Decoding with online tools
    Given I have the downloaded image
    Then I look for online stenography tools
    Then I enter the website
    """
    https://manytools.org/hacker-tools/steganography
    -encode-text-into-image/
    """
    But I can't found solution

  Scenario: Fail:Using Commands
    Given I have the image in PNG format
    Then I thought that the true file was type txt
    Then I try to see the content in hexadecimal
    And I use the hexdump -C command
    But I did not find any character that gave me the message

  Scenario: Success: HBS format
    Given In the site of the page gives a clue to about the colors
    Then I look for methods to hide the message through the colors
    """
    Using colors format(HBS,RGB) colors are represented by numbers
    by converting these numbers in ASCII format you can
    decrypt the message
    """
    Then I use IllustratorCs6
    And I open the image
    Then I use HBS color format
    """
    I change the format of the color palette to HBS
    """
    Then I use dropper tool to see the HBS colors in the palette
    Then I see in HBS format the colors of the image
    Then I keep the numbers of each color
    """
    83,79,76,85,84,73,79,78,32,73,83,32,67,79,76,79,82,45,
    83,72,69,77,69,83
    """
    And I convert them to ascii
    """
    SOLUTION IS COLOR-SHEMES
    """
    And I writte the message
    """
    COLOR-SHEMES
    """
    And I solve the challenge
