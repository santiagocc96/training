# language: en

Feature: Solve the challenge Try to see
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input

  Scenario: Successful solution
    When I look into the source code
    Then I see the submit calls an external javascript
    When I look into the source of the external javascript
    Then I see a var npwd
    Then I write the value of npwd in the input
    And I solve the challenge
