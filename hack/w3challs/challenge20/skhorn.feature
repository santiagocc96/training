# language: en

Feature: Solve challenge 20
  From site W3Challs
  From Cryptography Category
  With my username Skhorn

  Background:
    Given the fact i have an account on W3Challs site
    And i have Debian 9 as Operating System
    And i have internet access
    And i have no background on RSA

  Scenario: Succesful Solution
    Given the link to the challenge
    And a crypted text
    And a modulus N
    And a public exponential E
    When i search for RSA theory
    And i read it
    And i see that i need to factor N into its P and Q prime factor
    And i do this by starting just below the square root of N
    And i code a script to help me with this calculation
    And i use this script to calculate iteratively N mod sqrt[N]-1
    And i use the one that gives 0 as result as P
    And i code in the same script another function to calculate Q
    And i use the script to calculate N/P to get Q
    And next i should find phi(n) which it is (P-1)*(Q-1)
    And i code in the script another function to help me with it
    And i need to calculate the modular multiplicative inverse of E (E^-1)
    And i reuse some existing code to help with this task
    And in order to find the decryption key D i use the previous code
    When i get the decryption key D
    And i code a function to decrypt the ciphertext given the formula M = C^D mod N
    And i know that M stands for the message expected
    And i know that C stands for the ciphertext
    And i know that D stands for Decryption Key
    And i know that N stands for the already given modulus N
    And i use the script to get the plaintext
    When i see the plaintext 
    And i see it is pure numbers
    And i convert the output to hexadecimal
    And i code in the script a function for this task
    And i use the script to get the plaintext in hexadecimal
    And i convert this into ascii
    And i code in the script a function for this task
    And i use the script to convert it to ascii
    Then i should get a readable text
    And i am able to read the whole message including the password
    And i input the extracted password 
    And i solve the challenge
